package com.example.myapplication.ws;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Looper;
import com.xiaoxin.sdk.utils.Log;

import com.example.myapplication.ws.listener.WsStatusListener;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;

/**
 * @author rabtman
 */

public class WsManager implements IWsManager {

    private final static int RECONNECT_TIME = 5 * 1000;    //重连间隔
    private Context mContext;
    private String wsUrl;
    private WebSocket mWebSocket;
    private OkHttpClient mOkHttpClient;
    private Request mRequest;
    private int mCurrentStatus = WsStatus.DISCONNECTED;     //websocket连接状态
    private boolean isNeedReconnect;          //是否需要断线自动重连
    private boolean isManualClose = false;         //是否为手动关闭websocket连接
    private WsStatusListener wsStatusListener;
    private Lock mLock;
    private Handler wsMainHandler = new Handler(Looper.getMainLooper());
    private int reconnectCount = 0;   //重连次数
    private String TAG = "WsManager";
    private WebSocketListener mWebSocketListener = new WebSocketListener() {
        @Override
        public void onOpen(WebSocket webSocket, final Response response) {
            Log.d(TAG, "onOpen() called with: webSocket = [" + webSocket + "], response = [" + response + "]");
            mWebSocket = webSocket;
            setCurrentStatus(WsStatus.CONNECTED);
            connected();
            if (wsStatusListener != null) {
                handler(new Runnable() {
                    @Override
                    public void run() {
                        wsStatusListener.onOpen(response);
                    }
                });
            }
        }

        @Override
        public void onMessage(WebSocket webSocket, final ByteString bytes) {
            Log.d(TAG, "onMessage() called with: webSocket = [" + webSocket + "], bytes = [" + bytes + "]");
            if (wsStatusListener != null) {
                handler(new Runnable() {
                    @Override
                    public void run() {
                        wsStatusListener.onMessage(bytes);
                    }
                });
            }
        }

        @Override
        public void onMessage(WebSocket webSocket, final String text) {
            Log.d(TAG, "onMessage() called with: webSocket = [" + webSocket + "], text = [" + text + "]");
            if (wsStatusListener != null) {
                handler(new Runnable() {
                    @Override
                    public void run() {
                        wsStatusListener.onMessage(text);
                    }
                });
            }
        }

        @Override
        public void onClosing(WebSocket webSocket, final int code, final String reason) {
            Log.d(TAG, "onClosing() called with: webSocket = [" + webSocket + "], code = [" + code + "], reason = [" + reason + "]");
            if (wsStatusListener != null) {
                handler(new Runnable() {
                    @Override
                    public void run() {
                        wsStatusListener.onClosing(code, reason);
                    }
                });
            }
        }

        @Override
        public void onClosed(WebSocket webSocket, final int code, final String reason) {
            Log.d(TAG, "onClosed() called with: webSocket = [" + webSocket + "], code = [" + code + "], reason = [" + reason + "]");
            if (wsStatusListener != null) {
                handler(new Runnable() {
                    @Override
                    public void run() {
                        wsStatusListener.onClosed(code, reason);
                    }
                });
            }
        }

        @Override
        public void onFailure(WebSocket webSocket, final Throwable t, final Response response) {
            Log.d(TAG, "onFailure() called with: webSocket = [" + webSocket + "], t = [" + t + "], response = [" + response + "]");
            tryReconnect();
            if (wsStatusListener != null) {
                handler(new Runnable() {
                    @Override
                    public void run() {
                        wsStatusListener.onFailure(t, response);
                    }
                });
            }
        }
    };
    private Runnable reconnectRunnable = new Runnable() {
        @Override
        public void run() {
            if (wsStatusListener != null) {
                wsStatusListener.onReconnect();
            }
            buildConnect();
        }
    };

    public WsManager(Builder builder) {
        mContext = builder.mContext;
        wsUrl = builder.wsUrl;
        isNeedReconnect = builder.needReconnect;
        mOkHttpClient = builder.mOkHttpClient;
        this.mLock = new ReentrantLock();
    }

    private void handler(Runnable runnable) {
        if (Looper.myLooper() != Looper.getMainLooper()) {
            wsMainHandler.post(runnable);
        } else {
            runnable.run();
        }
    }

    private void initWebSocket() {
        Log.d(TAG, "initWebSocket() called");
        if (mOkHttpClient == null) {
            mOkHttpClient = new OkHttpClient.Builder()
                    .retryOnConnectionFailure(true)
                    .build();
        }
        if (mRequest == null) {
            mRequest = new Request.Builder()
                    .url(wsUrl)
                    .build();
        }
        mOkHttpClient.dispatcher().cancelAll();
        try {
            mLock.lockInterruptibly();
            try {
                mOkHttpClient.newWebSocket(mRequest, mWebSocketListener);
            } finally {
                mLock.unlock();
            }
        } catch (InterruptedException e) {
        }
    }

    @Override
    public WebSocket getWebSocket() {
        return mWebSocket;
    }


    public void setWsStatusListener(WsStatusListener wsStatusListener) {
        this.wsStatusListener = wsStatusListener;
    }

    @Override
    public synchronized boolean isWsConnected() {
        return mCurrentStatus == WsStatus.CONNECTED;
    }

    @Override
    public synchronized int getCurrentStatus() {
        return mCurrentStatus;
    }

    @Override
    public synchronized void setCurrentStatus(int currentStatus) {
        this.mCurrentStatus = currentStatus;
    }

    @Override
    public void startConnect() {
        Log.d(TAG, "startConnect() called");
        isManualClose = false;
        buildConnect();
    }

    @Override
    public void stopConnect() {
        Log.d(TAG, "stopConnect() called");
        isManualClose = true;
        disconnect();
    }

    private void tryReconnect() {
        Log.d(TAG, "tryReconnect() called");
        if (!isNeedReconnect | isManualClose) {
            Log.d(TAG, "tryReconnect() close");
            return;
        }

        if (!isNetworkConnected(mContext)) {
            Log.d(TAG, "tryReconnect() called no net");
            setCurrentStatus(WsStatus.DISCONNECTED);
            wsMainHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    tryReconnect();
                }
            }, RECONNECT_TIME);
            return;
        }

        setCurrentStatus(WsStatus.RECONNECT);
        wsMainHandler
                .postDelayed(reconnectRunnable, 1000 * 3);
        reconnectCount++;
    }

    private void cancelReconnect() {
        Log.d(TAG, "cancelReconnect() called");
        wsMainHandler.removeCallbacks(reconnectRunnable);
        reconnectCount = 0;
    }

    private void connected() {
        Log.d(TAG, "connected() called");
        cancelReconnect();
    }

    private void disconnect() {
        Log.d(TAG, "disconnect() called");
        if (mCurrentStatus == WsStatus.DISCONNECTED) {
            return;
        }
        cancelReconnect();
        if (mOkHttpClient != null) {
            mOkHttpClient.dispatcher().cancelAll();
        }
        if (mWebSocket != null) {
            boolean isClosed = mWebSocket.close(WsStatus.CODE.NORMAL_CLOSE, WsStatus.TIP.NORMAL_CLOSE);
            //非正常关闭连接
            if (!isClosed) {
                if (wsStatusListener != null) {
                    wsStatusListener.onClosed(WsStatus.CODE.ABNORMAL_CLOSE, WsStatus.TIP.ABNORMAL_CLOSE);
                }
            }
        }
        setCurrentStatus(WsStatus.DISCONNECTED);
    }

    private synchronized void buildConnect() {
        Log.d(TAG, "buildConnect() called " + getCurrentStatus());
        if (!isNetworkConnected(mContext)) {
            tryReconnect();
            setCurrentStatus(WsStatus.DISCONNECTED);
            return;
        }
        switch (getCurrentStatus()) {
            case WsStatus.CONNECTED:
            case WsStatus.CONNECTING:
                break;
            default:
                setCurrentStatus(WsStatus.CONNECTING);
                initWebSocket();
        }
    }

    //发送消息
    @Override
    public boolean sendMessage(String msg) {
        return send(msg);
    }

    @Override
    public boolean sendMessage(ByteString byteString) {
        return send(byteString);
    }

    private boolean send(Object msg) {
        boolean isSend = false;
        if (mWebSocket != null && mCurrentStatus == WsStatus.CONNECTED) {
            if (msg instanceof String) {
                isSend = mWebSocket.send((String) msg);
            } else if (msg instanceof ByteString) {
                isSend = mWebSocket.send((ByteString) msg);
            }
            //发送消息失败，尝试重连
            if (!isSend) {
                tryReconnect();
            }
        }
        return isSend;
    }

    //检查网络是否连接
    private boolean isNetworkConnected(Context context) {
        if (context != null) {
            ConnectivityManager mConnectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetworkInfo = mConnectivityManager
                    .getActiveNetworkInfo();
            if (mNetworkInfo != null) {
                return mNetworkInfo.isAvailable();
            }
        }
        return false;
    }

    public static final class Builder {

        private Context mContext;
        private String wsUrl;
        private boolean needReconnect = true;
        private OkHttpClient mOkHttpClient;

        public Builder(Context val) {
            mContext = val;
        }

        public Builder wsUrl(String val) {
            wsUrl = val;
            return this;
        }

        public Builder client(OkHttpClient val) {
            mOkHttpClient = val;
            return this;
        }

        public Builder needReconnect(boolean val) {
            needReconnect = val;
            return this;
        }

        public WsManager build() {
            return new WsManager(this);
        }
    }
}
