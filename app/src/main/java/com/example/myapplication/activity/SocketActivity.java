package com.example.myapplication.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import com.xiaoxin.sdk.utils.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.myapplication.R;
import com.example.myapplication.ws.WsManager;
import com.example.myapplication.ws.listener.WsStatusListener;
import com.xiaoxin.sdk.Xiaoxin;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Response;
import okio.ByteString;

public class SocketActivity extends AppCompatActivity {

    private static final String TAG = "SocketActivity ";
    private Button start;
    private TextView output;
    private WsManager wsManager;

    private WsStatusListener wsStatusListener = new WsStatusListener() {
        @Override
        public void onOpen(Response response) {
            super.onOpen(response);
            JSONObject json = new JSONObject();
            try {
                json.put("action", "register");
                json.put("uniqueid", Xiaoxin.getUniqueid());
                json.put("name", "AndroidWebsocket");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            wsManager.sendMessage(json.toString());
            Log.d(TAG, "onOpen() called with: response = [" + response + "]");
        }

        @Override
        public void onMessage(String text) {
            super.onMessage(text);
            output(text);
            Log.d(TAG, "onMessage() called with: text = [" + text + "]");
        }

        @Override
        public void onMessage(ByteString bytes) {
            super.onMessage(bytes);
            output(bytes.hex());
            Log.d(TAG, "onMessage() called with: bytes = [" + bytes.utf8() + "]");
        }

        @Override
        public void onReconnect() {
            super.onReconnect();
            Log.d(TAG, "onReconnect() called");
        }

        @Override
        public void onClosing(int code, String reason) {
            super.onClosing(code, reason);
            Log.d(TAG, "onClosing() called with: code = [" + code + "], reason = [" + reason + "]");
        }

        @Override
        public void onClosed(int code, String reason) {
            super.onClosed(code, reason);
            Log.d(TAG, "onClosed() called with: code = [" + code + "], reason = [" + reason + "]");
        }

        @Override
        public void onFailure(Throwable t, Response response) {
            super.onFailure(t, response);
            Log.d(TAG, "onFailure() called with: t = [" + t + "], response = [" + response + "]");
        }
    };


    private void output(final String txt) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                output.setText(output.getText().toString() + "\n" + txt);
            }
        });
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_socket);
        init();
    }

    private void init() {
        start = (Button) findViewById(R.id.start);
        output = (TextView) findViewById(R.id.output);
        wsManager = new WsManager.Builder(this)
                .client(new OkHttpClient()
                        .newBuilder()
                        .pingInterval(15, TimeUnit.SECONDS)
                        .build())
                .needReconnect(true)
                .wsUrl("ws://192.168.30.250:9990")
                .build();
        wsManager.setWsStatusListener(wsStatusListener);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wsManager.startConnect();
            }
        });

        findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wsManager.stopConnect();
            }
        });

        findViewById(R.id.send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wsManager.sendMessage("test");
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        wsManager.stopConnect();
    }
}
