package com.xiaoxin.sdk.data;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by zhangzhi on 16/8/30.
 */
public class GPS implements Parcelable {
    @SerializedName("x")
    public double x;
    @SerializedName("y")
    public double y;
    @Nullable
    @SerializedName("type")
    public String type;
    @Nullable
    @SerializedName("address")
    public String address;

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    @Nullable
    public String getType() {
        return type;
    }

    public void setType(@Nullable String type) {
        this.type = type;
    }

    @Nullable
    public String getAddress() {
        return address;
    }

    public void setAddress(@Nullable String address) {
        this.address = address;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeDouble(this.x);
        dest.writeDouble(this.y);
        dest.writeString(this.type);
        dest.writeString(this.address);
    }

    public GPS() {
    }

    protected GPS(Parcel in) {
        this.x = in.readDouble();
        this.y = in.readDouble();
        this.type = in.readString();
        this.address = in.readString();
    }

    public static final Parcelable.Creator<GPS> CREATOR = new Parcelable.Creator<GPS>() {
        @Override
        public GPS createFromParcel(@NonNull Parcel source) {
            return new GPS(source);
        }

        @Override
        public GPS[] newArray(int size) {
            return new GPS[size];
        }
    };


    @NonNull
    @Override
    public String toString() {
        return "GPS{" +
                "x=" + x +
                ", y=" + y +
                ", type='" + type + '\'' +
                ", address='" + address + '\'' +
                '}';
    }

}
