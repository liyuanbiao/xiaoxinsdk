package com.xiaoxin.sdk.data;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

public enum Sex {
    @SerializedName("male")
    male("男"),
    @SerializedName("female")
    female("女");
    private String sex;

    Sex(String sex) {
        this.sex = sex;
    }

    @NonNull
    @Override
    public String toString() {
        return "Sex{" + sex + "}";
    }
}
