package com.xiaoxin.sdk.data;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RestrictTo;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.xiaoxin.sdk.http.Api;
import com.xiaoxin.sdk.internal.rong.User;

import java.util.Date;
import java.util.List;

import io.rong.imlib.model.UserInfo;

/**
 * Created by ljman on 2018/4/9.
 */

public class Person implements Parcelable {
    @Nullable
    @SerializedName("id")
    private String id;
    @Nullable
    @SerializedName("channel")
    private String channel;
    @Nullable
    @SerializedName("uniqueid")
    private String uniqueid;
    @Nullable
    @SerializedName("token")
    private String token;
    @Nullable
    @SerializedName("rongToken")
    private String rongToken;
    @Nullable
    @SerializedName("name")
    private String name;
    @Nullable
    @SerializedName("sex")
    private Sex sex;
    @Nullable
    @SerializedName("birthDay")
    private Date birthDay;
    @Nullable
    @SerializedName("headImg")
    private String headImg;
    @Nullable
    @SerializedName("mobile")
    private String mobile;
    @Nullable
    @SerializedName("imei")
    private String imei;
    @Nullable
    @SerializedName("friends")
    private List<String> friends;

    @Nullable
    public String getId() {
        return id;
    }

    public void setId(@Nullable String id) {
        this.id = id;
    }

    @Nullable
    public String getToken() {
        return token;
    }

    public void setToken(@Nullable String token) {
        this.token = token;
    }

    @Nullable
    public String getRongToken() {
        return rongToken;
    }

    public void setRongToken(@Nullable String rongToken) {
        this.rongToken = rongToken;
    }

    @Nullable
    public String getName() {
        return name;
    }

    public void setName(@Nullable String name) {
        this.name = name;
    }

    @Nullable
    public Sex getSex() {
        return sex;
    }

    public void setSex(@Nullable Sex sex) {
        this.sex = sex;
    }

    @Nullable
    public Date getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(@Nullable Date birthDay) {
        this.birthDay = birthDay;
    }

    @Nullable
    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(@Nullable String headImg) {
        this.headImg = headImg;
    }

    @Nullable
    public String getMobile() {
        return mobile;
    }

    public void setMobile(@Nullable String mobile) {
        this.mobile = mobile;
    }

    @Nullable
    public String getImei() {
        return imei;
    }

    public void setImei(@Nullable String imei) {
        this.imei = imei;
    }

    @Nullable
    public List<String> getFriends() {
        return friends;
    }

    public void setFriends(@Nullable List<String> friends) {
        this.friends = friends;
    }

    @Nullable
    public String getChannel() {
        return channel;
    }

    public void setChannel(@Nullable String channel) {
        this.channel = channel;
    }

    @Nullable
    public String getUniqueid() {
        return uniqueid;
    }

    public void setUniqueid(@Nullable String uniqueid) {
        this.uniqueid = uniqueid;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.token);
        dest.writeString(this.rongToken);
        dest.writeString(this.name);
        dest.writeInt(this.sex == null ? -1 : this.sex.ordinal());
        dest.writeLong(this.birthDay != null ? this.birthDay.getTime() : -1);
        dest.writeString(this.headImg);
        dest.writeString(this.mobile);
        dest.writeString(this.imei);
        dest.writeStringList(this.friends);
        dest.writeString(this.channel);
        dest.writeString(this.uniqueid);
    }

    public Person() {
    }

    protected Person(Parcel in) {
        this.id = in.readString();
        this.token = in.readString();
        this.rongToken = in.readString();
        this.name = in.readString();
        int tmpSex = in.readInt();
        this.sex = tmpSex == -1 ? null : Sex.values()[tmpSex];
        long tmpBirthDay = in.readLong();
        this.birthDay = tmpBirthDay == -1 ? null : new Date(tmpBirthDay);
        this.headImg = in.readString();
        this.mobile = in.readString();
        this.imei = in.readString();
        this.friends = in.createStringArrayList();
        this.channel = in.readString();
        this.uniqueid = in.readString();
    }

    public static final Creator<Person> CREATOR = new Creator<Person>() {
        @Override
        public Person createFromParcel(@NonNull Parcel source) {
            return new Person(source);
        }

        @Override
        public Person[] newArray(int size) {
            return new Person[size];
        }
    };

    @NonNull
    @Override
    public String toString() {
        return "Person{" +
                "id='" + id + '\'' +
                ", token='" + token + '\'' +
                ", rongToken='" + rongToken + '\'' +
                ", name='" + name + '\'' +
                ", sex=" + sex +
                ", birthDay=" + birthDay +
                ", headImg='" + headImg + '\'' +
                ", mobile='" + mobile + '\'' +
                ", imei='" + imei + '\'' +
                ", friends=" + friends +
                ", channel='" + channel + '\'' +
                ", uniqueid='" + uniqueid + '\'' +
                '}';
    }

    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        return id != null ? id.equals(person.id) : person.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @RestrictTo(RestrictTo.Scope.LIBRARY)
    public UserInfo toUserInfo() {
        Uri portraitUri = TextUtils.isEmpty(headImg) ? null : Uri.parse(Api.IMAGE_PATH + headImg);
        return new UserInfo(getId(), getName(), portraitUri);
    }

    @RestrictTo(RestrictTo.Scope.LIBRARY)
    public User toUser() {
        Uri portraitUri = TextUtils.isEmpty(headImg) ? null : Uri.parse(Api.IMAGE_PATH + headImg);
        return new User(getId(), getName(), portraitUri);
    }
}
