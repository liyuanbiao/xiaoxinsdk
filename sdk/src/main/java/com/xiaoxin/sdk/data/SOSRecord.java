package com.xiaoxin.sdk.data;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by zhangzhi on 16/9/1.
 */
//混用 sosRecordLocation 里面有owner未接受
public class SOSRecord implements Parcelable {
    /**
     * status : closed
     * createdAt : 2016-08-17T02:50:15.165Z
     * updatedAt : 2016-08-20T10:14:28.146Z
     * endDate : 2016-08-20T10:14:28.145Z
     * id : 57b3d0e7cd5b067161b476f8
     */
    public enum Status {
        @SerializedName("closed")
        closed,
        @SerializedName("open")
        open,
        @SerializedName("stopRecording")
        stopRecording
    }

    @Nullable
    @SerializedName("person")
    private String person;
    @Nullable
    @SerializedName("location")
    private GPS location;
    @Nullable
    @SerializedName("status")
    private Status status;
    @Nullable
    @SerializedName("createdAt")
    private Date createdAt;
    @Nullable
    @SerializedName("endDate")
    private Date endDate;
    @Nullable
    @SerializedName("id")
    private String id;
    @Nullable
    @SerializedName("audio")
    private String audio;

    public void setPerson(@Nullable String personId) {
        person = personId;
    }

    @Nullable
    public String getPerson() {
        return person;
    }

    @Nullable
    public String getAudio() {
        return audio;
    }

    public void setAudio(@Nullable String audio) {
        this.audio = audio;
    }

    @Nullable
    public GPS getLocation() {
        return location;
    }

    public void setLocation(@Nullable GPS location) {
        this.location = location;
    }

    @Nullable
    public Status getStatus() {
        return status;
    }

    public void setStatus(@Nullable Status status) {
        this.status = status;
    }

    @Nullable
    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(@Nullable Date createdAt) {
        this.createdAt = createdAt;
    }

    @Nullable
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(@Nullable Date endDate) {
        this.endDate = endDate;
    }

    @Nullable
    public String getId() {
        return id;
    }

    public void setId(@Nullable String id) {
        this.id = id;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeString(this.person);
        dest.writeParcelable(this.location, flags);
        dest.writeInt(this.status == null ? -1 : this.status.ordinal());
        dest.writeLong(this.createdAt != null ? this.createdAt.getTime() : -1);
        dest.writeLong(this.endDate != null ? this.endDate.getTime() : -1);
        dest.writeString(this.id);
        dest.writeString(this.audio);
    }

    public SOSRecord() {
    }

    protected SOSRecord(Parcel in) {
        this.person = in.readString();
        this.location = in.readParcelable(GPS.class.getClassLoader());
        int tmpStatus = in.readInt();
        this.status = tmpStatus == -1 ? null : Status.values()[tmpStatus];
        long tmpCreatedAt = in.readLong();
        this.createdAt = tmpCreatedAt == -1 ? null : new Date(tmpCreatedAt);
        long tmpEndDate = in.readLong();
        this.endDate = tmpEndDate == -1 ? null : new Date(tmpEndDate);
        this.id = in.readString();
        this.audio = in.readString();
    }

    public static final Parcelable.Creator<SOSRecord> CREATOR = new Parcelable.Creator<SOSRecord>() {
        @Override
        public SOSRecord createFromParcel(@NonNull Parcel source) {
            return new SOSRecord(source);
        }

        @Override
        public SOSRecord[] newArray(int size) {
            return new SOSRecord[size];
        }
    };


    @NonNull
    @Override
    public String toString() {
        return "SOSRecord{" +
                "person='" + person + '\'' +
                ", location=" + location +
                ", status=" + status +
                ", createdAt=" + createdAt +
                ", endDate=" + endDate +
                ", id='" + id + '\'' +
                ", audio='" + audio + '\'' +
                '}';
    }

    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SOSRecord sosRecord = (SOSRecord) o;

        return id != null ? id.equals(sosRecord.id) : sosRecord.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
