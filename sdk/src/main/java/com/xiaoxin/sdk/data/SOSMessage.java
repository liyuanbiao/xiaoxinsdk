package com.xiaoxin.sdk.data;

import android.os.Parcel;
import android.support.annotation.NonNull;
import com.xiaoxin.sdk.utils.Log;

import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.Charset;

import io.rong.common.ParcelUtils;
import io.rong.common.RLog;
import io.rong.imlib.MessageTag;
import io.rong.imlib.model.MessageContent;
import io.rong.imlib.model.UserInfo;

/**
 * Created by zhangzhi on 16/9/9.
 */

@MessageTag(value = "SosCallNtf", flag = MessageTag.NONE)
public class SOSMessage extends MessageContent {
    private static final String TAG = "SOSMessage";

    @SerializedName("operation")
    private String operation;
    @SerializedName("sourceUserId")
    private String sourceUserId;
    @SerializedName("targetUserId")
    private String targetUserId;
    @SerializedName("message")
    private String message;
    @SerializedName("extra")
    private String extra;

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getSourceUserId() {
        return sourceUserId;
    }

    public void setSourceUserId(String sourceUserId) {
        this.sourceUserId = sourceUserId;
    }

    public String getTargetUserId() {
        return targetUserId;
    }

    public void setTargetUserId(String targetUserId) {
        this.targetUserId = targetUserId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public static SOSMessage obtain() {
        return new SOSMessage();
    }

    @NonNull
    public static SOSMessage obtain(String operation,
                                    String sourceUserId,
                                    String targetUserId,
                                    String message) {
        SOSMessage obj = new SOSMessage();
        obj.operation = operation;
        obj.sourceUserId = sourceUserId;
        obj.targetUserId = targetUserId;
        obj.message = message;
        return obj;
    }


    private SOSMessage() {
    }

    public byte[] encode() {
        JSONObject jsonObj = new JSONObject();

        try {
            jsonObj.putOpt("operation", this.operation);
            jsonObj.putOpt("sourceUserId", this.sourceUserId);
            jsonObj.putOpt("targetUserId", this.targetUserId);
            jsonObj.putOpt("message", this.message);
            jsonObj.putOpt("extra", this.getExtra());
            jsonObj.putOpt("user", this.getJSONUserInfo());
        } catch (JSONException var4) {
            RLog.e("ContactNotificationMessage", "JSONException " + var4.getMessage());
        }

        Log.w(TAG, "encode: " + jsonObj.toString());
        return jsonObj.toString().getBytes(Charset.forName("UTF-8"));
    }

    public SOSMessage(byte[] data) {
        String jsonStr = new String(data, Charset.forName("UTF-8"));

        try {
            JSONObject e = new JSONObject(jsonStr);
            this.setOperation(e.optString("operation"));
            this.setSourceUserId(e.optString("sourceUserId"));
            this.setMessage(e.optString("message"));
            this.setExtra(e.optString("extra"));
            if (e.has("user")) {
                this.setUserInfo(this.parseJsonToUserInfo(e.getJSONObject("user")));
            }
        } catch (JSONException exception) {
            Log.e(TAG, "JSONException " + exception.getMessage());
        }
    }

    public SOSMessage(@NonNull Parcel in) {
        this.operation = ParcelUtils.readFromParcel(in);
        this.sourceUserId = ParcelUtils.readFromParcel(in);
        this.targetUserId = ParcelUtils.readFromParcel(in);
        this.message = ParcelUtils.readFromParcel(in);
        this.extra = ParcelUtils.readFromParcel(in);
        this.setUserInfo(ParcelUtils.readFromParcel(in, UserInfo.class));
    }

    public void writeToParcel(@NonNull Parcel dest, int flags) {
        ParcelUtils.writeToParcel(dest, this.operation);
        ParcelUtils.writeToParcel(dest, this.sourceUserId);
        ParcelUtils.writeToParcel(dest, this.targetUserId);
        ParcelUtils.writeToParcel(dest, this.message);
        ParcelUtils.writeToParcel(dest, this.extra);
        ParcelUtils.writeToParcel(dest, this.getUserInfo());
    }

    public int describeContents() {
        return 0;
    }

    public static final Creator<SOSMessage> CREATOR = new Creator<SOSMessage>() {
        @Override
        public SOSMessage createFromParcel(@NonNull Parcel source) {
            return new SOSMessage(source);
        }

        @Override
        public SOSMessage[] newArray(int size) {
            return new SOSMessage[size];
        }

    };

    @NonNull
    @Override
    public String toString() {
        return "SOSMessage{" +
                "operation='" + operation + '\'' +
                ", sourceUserId='" + sourceUserId + '\'' +
                ", targetUserId='" + targetUserId + '\'' +
                ", message='" + message + '\'' +
                ", extra='" + extra + '\'' +
                '}';
    }
}
