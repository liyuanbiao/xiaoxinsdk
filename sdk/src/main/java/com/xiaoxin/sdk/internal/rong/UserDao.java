package com.xiaoxin.sdk.internal.rong;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.support.annotation.NonNull;

@Dao
public interface UserDao {
    @Query("SELECT * FROM USER WHERE ID = :id")
    User findUserById(@NonNull String id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void save(@NonNull User user);

}
