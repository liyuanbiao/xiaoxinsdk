package com.xiaoxin.sdk.internal.rong;

import android.arch.persistence.room.TypeConverter;
import android.net.Uri;
import android.text.TextUtils;

public class Converters {
    @TypeConverter
    public String uri2String(Uri uri) {
        if (uri == null) {
            return null;
        }
        return uri.toString();
    }

    @TypeConverter
    public Uri string2Uri(String uriString) {
        if (TextUtils.isEmpty(uriString)) {
            return null;
        }
        return Uri.parse(uriString);
    }
}
