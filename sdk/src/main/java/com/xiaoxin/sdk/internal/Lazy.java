package com.xiaoxin.sdk.internal;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;

class Lazy<T> {
    @Nullable
    private T t;
    private AtomicBoolean flag = new AtomicBoolean(false);
    @NonNull
    private Callable<T> callable;

    Lazy(@NonNull Callable<T> callable) {
        Objects.requireNonNull(callable, "Lazy callable must not be null");
        this.callable = callable;
    }

    @Nullable
    T get() {
        try {
            if (flag.compareAndSet(false, true)) {
                t = callable.call();
            }
            return t;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
