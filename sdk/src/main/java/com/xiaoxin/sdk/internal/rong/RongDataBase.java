package com.xiaoxin.sdk.internal.rong;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

@TypeConverters(Converters.class)
@Database(entities = {User.class}, version = 1, exportSchema = false)
public abstract class RongDataBase extends RoomDatabase {
    abstract UserDao getUserDao();
}
