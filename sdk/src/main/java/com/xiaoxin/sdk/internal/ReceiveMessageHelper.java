package com.xiaoxin.sdk.internal;


import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import io.rong.imlib.RongIMClient;
import io.rong.imlib.model.Message;

public class ReceiveMessageHelper implements RongIMClient.OnReceiveMessageListener {
    @NonNull
    private static final ReceiveMessageHelper instance = new ReceiveMessageHelper();

    @NonNull
    public static ReceiveMessageHelper getInstance() {
        return instance;
    }

    @NonNull
    private List<RongIMClient.OnReceiveMessageListener> list = new ArrayList<>();

    public boolean hasListener(@NonNull RongIMClient.OnReceiveMessageListener onReceiveMessageListener) {
        return list.contains(onReceiveMessageListener);
    }

    public void registerOnReceiveMessageListener(
            @NonNull RongIMClient.OnReceiveMessageListener onReceiveMessageListener
    ) {
        Objects.requireNonNull(onReceiveMessageListener);
        if (!list.contains(onReceiveMessageListener)) {
            list.add(onReceiveMessageListener);
        }
    }

    public void unregisterOnReceiveMessageListener(
            @NonNull RongIMClient.OnReceiveMessageListener onReceiveMessageListener
    ) {
        Objects.requireNonNull(onReceiveMessageListener);
        list.remove(onReceiveMessageListener);
    }

    @Override
    public boolean onReceived(@NonNull Message message, int left) {
        boolean flag = false;
        for (int i = list.size() - 1; i >= 0; i--) {
            flag |= list.get(i).onReceived(message, left);
        }
        return flag;
    }
}
