package com.xiaoxin.sdk.internal;

import android.support.annotation.NonNull;

import io.rong.imlib.model.Message;

public class MessageReceived {
    @NonNull
    private Message message;
    private int left;

    public MessageReceived(@NonNull Message message, int left) {
        this.message = message;
        this.left = left;
    }

    @NonNull
    public Message getMessage() {
        return message;
    }

    public int getLeft() {
        return left;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MessageReceived that = (MessageReceived) o;

        if (left != that.left) return false;
        return message.equals(that.message);
    }

    @Override
    public int hashCode() {
        int result = message.hashCode();
        result = 31 * result + left;
        return result;
    }

    @NonNull
    @Override
    public String toString() {
        return "ReceivedData{" +
                "message=" + message +
                ", left=" + left +
                '}';
    }
}
