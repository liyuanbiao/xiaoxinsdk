package com.xiaoxin.sdk.internal.rong;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.LruCache;

import com.xiaoxin.sdk.Xiaoxin;
import com.xiaoxin.sdk.data.Person;
import com.xiaoxin.sdk.http.Api;
import com.xiaoxin.sdk.internal.DataBaseUtil;
import com.xiaoxin.sdk.utils.rong.RongIMUtil;

import java.util.NoSuchElementException;

import io.reactivex.Maybe;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import io.rong.imkit.RongIM;
import io.rong.imlib.model.UserInfo;

public class UserProvider implements RongIM.UserInfoProvider {
    private UserProvider() {
    }

    private static final UserProvider instance = new UserProvider();

    public static UserProvider getInstance() {
        return instance;
    }

    private LruCache<String, UserInfo> lruCache =
            new LruCache<String, UserInfo>(100) {
                @Nullable
                @Override
                protected UserInfo create(@NonNull String key) {
                    User user = DataBaseUtil.getRongDataBase()
                            .getUserDao().findUserById(key);
                    return user != null ? user.toUserInfo() : null;
                }
            };

    @NonNull
    public Single<UserInfo> getUserInfoAsync(@NonNull final String id) {
        return Maybe.fromCallable(() -> lruCache.get(id)).toSingle()
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof NoSuchElementException) {
                        return getContactInfo(id);
                    } else {
                        return Single.error(throwable);
                    }
                }).subscribeOn(Schedulers.io());
    }

    private Single<UserInfo> getContactInfo(@NonNull String id) {
        return Api.service().getContactInfo(id, Xiaoxin.getAppKey())
                .doOnSuccess(RongIMUtil::refreshUserInfoCache)
                .map(Person::toUser)
                .doOnSuccess(DataBaseUtil.getRongDataBase().getUserDao()::save)
                .map(User::toUserInfo)
                .doOnSuccess(userInfo -> lruCache.put(id, userInfo));
    }

    @Nullable
    @Override
    public UserInfo getUserInfo(@NonNull String id) {
        try {
            return getUserInfoAsync(id).blockingGet();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
