package com.xiaoxin.sdk.internal;

import android.arch.persistence.room.Room;

import com.xiaoxin.sdk.Xiaoxin;
import com.xiaoxin.sdk.internal.rong.RongDataBase;

public class DataBaseUtil {
    private static Lazy<RongDataBase> rongDataBase = new Lazy<>(() ->
            Room.databaseBuilder(Xiaoxin.getContext(), RongDataBase.class, "rong_im.db")
                    .allowMainThreadQueries()
                    .build()
    );

    public static RongDataBase getRongDataBase() {
        return rongDataBase.get();
    }
}
