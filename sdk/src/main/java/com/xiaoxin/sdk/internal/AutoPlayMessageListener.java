package com.xiaoxin.sdk.internal;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.xiaoxin.library.rxaudio.RxMediaPlayer;
import com.xiaoxin.sdk.Xiaoxin;
import com.xiaoxin.sdk.internal.rong.UserProvider;
import com.xiaoxin.sdk.utils.Log;
import com.xiaoxin.xfyun.rxmsc.RxSpeechSynthesizer;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.processors.PublishProcessor;
import io.rong.imkit.emoticon.AndroidEmoji;
import io.rong.imlib.RongIMClient;
import io.rong.imlib.model.Message;
import io.rong.imlib.model.MessageContent;
import io.rong.imlib.model.UserInfo;
import io.rong.message.ImageMessage;
import io.rong.message.TextMessage;
import io.rong.message.VoiceMessage;

public class AutoPlayMessageListener implements RongIMClient.OnReceiveMessageListener {
    private static final String TAG = "AutoPlayMessageListener";
    @NonNull
    private PublishProcessor<MessageReceived> processor = PublishProcessor.create();

    private class UserMessage {
        UserInfo userInfo;
        Message message;

        UserMessage(UserInfo userInfo, Message message) {
            this.userInfo = userInfo;
            this.message = message;
        }
    }

    public AutoPlayMessageListener() {
        subscribeMessageReceived();
    }

    @SuppressLint("CheckResult")
    private void subscribeMessageReceived() {
        processor.onBackpressureLatest()
                .concatMapSingleDelayError(messageReceived -> {
                    Message message = messageReceived.getMessage();
                    String senderUserId = message.getSenderUserId();
                    return UserProvider.getInstance()
                            .getUserInfoAsync(senderUserId)
                            .zipWith(Single.just(message), UserMessage::new);
                })
                .concatMapCompletableDelayError(this::handleMessage)
                .subscribe();
    }

    @NonNull
    private Completable handleMessage(@NonNull UserMessage userMessage) {
        final UserInfo userInfo = userMessage.userInfo;
        final Message message = userMessage.message;
        final MessageContent content = message.getContent();

        Completable completable;
        if (content instanceof TextMessage) {
            completable = handleTextMessage(userInfo, ((TextMessage) content));
        } else if (content instanceof ImageMessage) {
            completable = handleImageMessage(userInfo, (ImageMessage) content);
        } else if (content instanceof VoiceMessage) {
            completable = handleVoiceMessage(userInfo, (VoiceMessage) content);
        } else {
            completable = Completable.complete();
        }
        return completable;
    }

    @SuppressLint("CheckResult")
    @Override
    public boolean onReceived(@NonNull Message message, int left) {
        MessageContent content = message.getContent();
        if (content instanceof TextMessage ||
                content instanceof ImageMessage ||
                content instanceof VoiceMessage) {
            processor.onNext(new MessageReceived(message, left));
            return true;
        }
        return false;
    }

    @NonNull
    private Completable handleVoiceMessage(@NonNull UserInfo userInfo,
                                           @NonNull VoiceMessage message) {
        Log.d(TAG, "handleVoiceMessage() called with: userInfo = [" + userInfo + "], message = [" + message + "]");
        Context context = Xiaoxin.getContext();
        Completable speak = RxSpeechSynthesizer.speak(context, userInfo.getName() + "说：");
        Completable play = RxMediaPlayer.startPlay(context, message.getUri());
        return speak.andThen(play);
    }

    @NonNull
    private Completable handleImageMessage(@NonNull UserInfo userInfo,
                                           @NonNull ImageMessage message) {
        Log.d(TAG, "handleImageMessage() called with: userInfo = [" + userInfo + "], message = [" + message + "]");
        String content = userInfo.getName() + "发来图片";
        return RxSpeechSynthesizer.speak(Xiaoxin.getContext(), content);
    }

    @NonNull
    private Completable handleTextMessage(@NonNull UserInfo userInfo,
                                          @NonNull TextMessage message) {
        Log.d(TAG, "handleTextMessage() called with: userInfo = [" + userInfo + "], message = [" + message + "]");
        String content = message.getContent();
        if (TextUtils.isEmpty(content)) {
            return Completable.complete();
        }
        StringBuilder sb = new StringBuilder();
        sb.append(userInfo.getName());
        if (AndroidEmoji.isEmoji(content)) {
            if (AndroidEmoji.getEmojiCount(content) * 2 == content.length()) {
                sb.append("发来表情");
            } else {
                sb.append("发来表情，并说：").append(content);
            }
        } else {
            sb.append("说：").append(content);
        }
        return RxSpeechSynthesizer.speak(Xiaoxin.getContext(), sb.toString());
    }
}