package com.xiaoxin.sdk.internal.rong;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import io.rong.imlib.model.UserInfo;

@Entity(tableName = "User")
public class User {
    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "id")
    private String id;
    @Nullable
    @ColumnInfo(name = "name")
    private String name;
    @Nullable
    @ColumnInfo(name = "portraitUri")
    private Uri portraitUri;

    public User() {
    }

    @Ignore
    public User(@NonNull String id) {
        this.id = id;
    }

    @Ignore
    public User(@NonNull String id, @Nullable String name, @Nullable Uri portraitUri) {
        this.id = id;
        this.name = name;
        this.portraitUri = portraitUri;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    @Nullable
    public String getName() {
        return name;
    }

    public void setName(@Nullable String name) {
        this.name = name;
    }

    @Nullable
    public Uri getPortraitUri() {
        return portraitUri;
    }

    public void setPortraitUri(@Nullable Uri portraitUri) {
        this.portraitUri = portraitUri;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return id.equals(user.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @NonNull
    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", portraitUri=" + portraitUri +
                '}';
    }

    @NonNull
    public UserInfo toUserInfo() {
        return new UserInfo(getId(), getName(), getPortraitUri());
    }

    @NonNull
    public static User fromUserInfo(@NonNull UserInfo userInfo) {
        return new User(userInfo.getUserId(), userInfo.getName(), userInfo.getPortraitUri());
    }
}
