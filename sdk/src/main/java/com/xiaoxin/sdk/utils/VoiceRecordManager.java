package com.xiaoxin.sdk.utils;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.xiaoxin.sdk.utils.Log;

import com.xiaoxin.sdk.Xiaoxin;

import java.io.File;

import io.rong.imlib.IRongCallback;
import io.rong.imlib.RongIMClient;
import io.rong.imlib.model.Conversation;
import io.rong.message.VoiceMessage;

/**
 * Created by liyuanbiao on 2017/9/19.
 */

public class VoiceRecordManager implements Handler.Callback {
    private static final int WHAT_START_RECORD = 0;
    private static final int WHAT_STOP_RECORD = 1;
    private static final int WHAT_CANCEL_RECORD = 2;

    public static final int STATUS_RECORD_START = 0X100;
    public static final int STATUS_RECORD_COMPLETE = 0X101;
    public static final int STATUS_RECORD_CANCEL = 0X102;
    public static final int STATUS_RECORD_ERROR = 0X103;
    public static final int STATUS_SEND_ATTACHED = 0X104;
    public static final int STATUS_SEND_SUCCESS = 0X105;
    public static final int STATUS_SEND_ERROR = 0X106;

    private static final String EXTRA_OUTPUT_FILE = "extra_output_file";

    private static final String TAG = "VoiceRecordManager";
    private static VoiceRecordManager manager;
    @NonNull
    private final ChatRecorder chatRecorder;
    private final Context context;
    @Nullable
    private HandlerThread handlerThread;
    private Handler handler;

    private String targetId;
    private Object targetValue;
    private Conversation.ConversationType conversationType;
    private onVoiceRecordListener onVoiceRecordListener;
    private IRongCallback.ISendMessageCallback sendMessageCallback;
    private boolean autoSend = true; //录音后是否自动发送信息
    private int minRecordDuration = 600; //最小录音时间

    private boolean initialized; //是否初始化

    private int status;

    public int getStatus() {
        return status;
    }

    private void setStatus(int status) {
        Log.d(TAG, "setStatus() called with: status = [" + status + "]");
        this.status = status;
    }

    public boolean isInitialized() {
        Log.d(TAG, "isInitialized() called -> " + initialized);
        return initialized;
    }

    private VoiceRecordManager(Context context) {
        this.context = context.getApplicationContext();
        chatRecorder = ChatRecorder.newInstance();
    }

    public boolean hasTarget() {
        return !TextUtils.isEmpty(getTargetId())
                && getConversationType() != null;
    }

    public void setTarget(String targetId, Conversation.ConversationType conversationType) {
        this.targetId = targetId;
        this.conversationType = conversationType;
    }

    public void setTargetValue(Object targetValue) {
        this.targetValue = targetValue;
    }

    public Object getTargetValue() {
        return targetValue;
    }

    /**
     * 初始化用户信息
     *
     * @param targetId         目标ID
     * @param conversationType 会话类型
     */
    public void init(final String targetId, Conversation.ConversationType conversationType,
                     onVoiceRecordListener onVoiceRecordListener,
                     IRongCallback.ISendMessageCallback sendMessageCallback) {

        this.onVoiceRecordListener = onVoiceRecordListener;
        this.sendMessageCallback = sendMessageCallback;
        init(targetId, conversationType);
    }

    public void init(final String targetId, Conversation.ConversationType conversationType) {
        this.targetId = targetId;
        this.conversationType = conversationType;
        init();
    }

    public void init() {
        chatRecorder.setOnStateChangeListener(onStateChangeListener);

        if (handlerThread == null || handler == null) {
            handlerThread = new HandlerThread(TAG);
            handlerThread.start();
            handler = new Handler(handlerThread.getLooper(), this);
        }

        initialized = true;
    }

    @NonNull
    private ChatRecorder.OnStateChangeListener onStateChangeListener = new ChatRecorder.OnStateChangeListener() {
        @Override
        public void onStateChange(int state) {
            if (state == ChatRecorder.STATE_RECORD_ERROR) {
                setStatus(STATUS_SEND_ERROR);
            }
        }
    };

    public void setOnVoiceRecordListener(VoiceRecordManager.onVoiceRecordListener onVoiceRecordListener) {
        this.onVoiceRecordListener = onVoiceRecordListener;
    }

    public void setSendMessageCallback(IRongCallback.ISendMessageCallback sendMessageCallback) {
        this.sendMessageCallback = sendMessageCallback;
    }

    /**
     * 释放资源
     */
    public void release() {
        exitRecorder();
        if (isInitialized()) {
            chatRecorder.setOnStateChangeListener(null);
            handler.removeMessages(WHAT_START_RECORD);
            handler.removeMessages(WHAT_STOP_RECORD);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                handlerThread.quitSafely();
            }else {
                handlerThread.quit();
            }
            handlerThread = null;
            initialized = false;
        }
    }

    public static VoiceRecordManager getInstance() {
        return getInstance(Xiaoxin.getContext());
    }

    public static VoiceRecordManager getInstance(@NonNull Context context) {
        if (manager == null) {
            synchronized (VoiceRecordManager.class) {
                if (manager == null) {
                    manager = new VoiceRecordManager(context);
                }
            }
        }
        return manager;
    }

    public static VoiceRecordManager newInstance(@NonNull Context context) {
        return new VoiceRecordManager(context);
    }

    @Override
    public boolean handleMessage(@NonNull Message msg) {
        switch (msg.what) {
            case WHAT_START_RECORD:
                Bundle data = msg.getData();
                String output = data.getString(EXTRA_OUTPUT_FILE);
                if (TextUtils.isEmpty(output)) {
                    setStatus(STATUS_RECORD_ERROR);
                    if (onVoiceRecordListener != null) {
                        onVoiceRecordListener.onRecordError(null,
                                ErrorCode.OUTPUT_FILE_EMPTY);
                    }
                    return true;
                }
                startRecord(output);
                setStatus(STATUS_RECORD_START);
                if (onVoiceRecordListener != null) {
                    onVoiceRecordListener.onRecordStart(output);
                }
                break;
            case WHAT_STOP_RECORD:

                String outputFileName = chatRecorder.getOutputFileName();
                if (TextUtils.isEmpty(outputFileName)) {
                    setStatus(STATUS_RECORD_ERROR);
                    if (onVoiceRecordListener != null) {
                        onVoiceRecordListener.onRecordError(null,
                                ErrorCode.OUTPUT_FILE_EMPTY);
                    }
                    return true;
                }

                long duration = stopRecord();
                if (duration < getMinRecordDuration()) {
                    setStatus(STATUS_RECORD_ERROR);
                    if (onVoiceRecordListener != null) {
                        onVoiceRecordListener.onRecordError(null,
                                ErrorCode.RECORD_DURATION_TOO_SHORT);
                    }
                    File file = new File(outputFileName);
                    if (file.exists() && file.delete()) {
                        Log.d(TAG, "handleMessage: 删除过短的声音文件");
                    }
                    return true;
                }
                setStatus(STATUS_RECORD_COMPLETE);
                if (onVoiceRecordListener != null) {
                    onVoiceRecordListener.onRecordComplete(outputFileName);
                }
                if (duration > 0 && isAutoSend()) {
                    VoiceMessage voiceMessage = makeVoiceMessage(outputFileName, (int) duration);
                    sendMessage(targetId, conversationType, voiceMessage);
                }
                break;
            case WHAT_CANCEL_RECORD:
                stopRecord();
                String oFileName = chatRecorder.getOutputFileName();
                File file = new File(oFileName);
                if (file.exists() && file.delete()) {
                    Log.d(TAG, "handleMessage: 取消录制，删除声音文件");
                }
                setStatus(STATUS_RECORD_CANCEL);
                if (onVoiceRecordListener != null) {
                    onVoiceRecordListener.onRecordCancel(oFileName);
                }
                break;
        }
        return true;
    }

    /**
     * 创建开始录音消息
     */
    private Message makeStartRecordMessage(String outputFileName) {
        Log.d(TAG, "makeStartRecordMessage() called with: outputFileName = [" + outputFileName + "]");
        Message message = Message.obtain();
        message.what = WHAT_START_RECORD;
        Bundle data = new Bundle();
        data.putString(EXTRA_OUTPUT_FILE, outputFileName);
        message.setData(data);
        return message;
    }

    /**
     * 创建停止录音消息
     */
    private Message makeStopRecordMessage() {
        Log.d(TAG, "makeStopRecordMessage() called");
        Message message = Message.obtain();
        message.what = WHAT_STOP_RECORD;
        return message;
    }

    /**
     * 创建停止录音消息
     */
    private Message makeCancelRecordMessage() {
        Log.d(TAG, "makeStopRecordMessage() called");
        Message message = Message.obtain();
        message.what = WHAT_CANCEL_RECORD;
        return message;
    }

    public void startRecordVoice() {
        Log.d(TAG, "startRecordVoice() called");
        if (isInitialized()) {
            String outputFileName = makeFileName();
            Message message = makeStartRecordMessage(outputFileName);
            handler.sendMessage(message);
        }
    }

    public void stopRecordVoice() {
        Log.d(TAG, "stopRecordVoice() called");
        if (isInitialized()) {
            Message message = makeStopRecordMessage();
            handler.sendMessage(message);
        }
    }

    public void cancelRecordVoice() {
        Log.d(TAG, "stopRecordVoice() called");
        if (isInitialized()) {
            Message message = makeCancelRecordMessage();
            handler.sendMessage(message);
        }
    }

    @NonNull
    private String makeFileName() {
        String id = targetId;
        if (!isInitialized()) {
            id = "not_initialized";
        }
        String fileName = id + "_" + System.currentTimeMillis() + ".amr";
        File audioDir;
        if (TextUtils.equals(Environment.getExternalStorageState(),
                Environment.MEDIA_MOUNTED)) {
            audioDir = new File(context.getExternalCacheDir(), "VoiceMessage");
        } else {
            audioDir = new File(context.getCacheDir(), "VoiceMessage");
        }
        String filePath = new File(audioDir, fileName).getAbsolutePath();
        Log.d(TAG, "makeFileName() called -> " + filePath);
        return filePath;
    }

    /**
     * 创建VoiceMessage
     *
     * @param filePath 文件路径
     * @param duration 毫秒
     * @return 声音消息
     */
    private static VoiceMessage makeVoiceMessage(String filePath, int duration) {
        Log.d(TAG, "makeVoiceMessage() called with: filePath = [" + filePath + "], duration = [" + duration + "]");
        if (!TextUtils.isEmpty(filePath)) {
            File voiceFile = new File(filePath);
            //创建语音消息，duration是s，但是传过来是ms
            duration = Math.round(duration * 1f / 1000);
            return VoiceMessage.obtain(Uri.fromFile(voiceFile), duration < 1 ? 1 : duration);
        }
        return null;
    }

    /**
     * 发送消息
     *
     * @param targetId     目标ID
     * @param voiceMessage 将要发送的消息
     */
    private void sendMessage(final String targetId, Conversation.ConversationType conversationType, VoiceMessage voiceMessage) {
        Log.d(TAG, "sendMessage() called with: targetId = [" + targetId + "], voiceMessage = [" + voiceMessage + "]");
        IRongCallback.ISendMessageCallback sendMessageCallback = new IRongCallback.ISendMessageCallback() {
            @Override
            public void onAttached(io.rong.imlib.model.Message message) {
                Log.d(TAG, "onAttached() called with: message = [" + message + "]");
                setStatus(STATUS_SEND_ATTACHED);
                if (VoiceRecordManager.this.sendMessageCallback != null) {
                    VoiceRecordManager.this.sendMessageCallback.onAttached(message);
                }
            }

            @Override
            public void onSuccess(io.rong.imlib.model.Message message) {
                Log.d(TAG, "onSuccess() called with: message = [" + message + "]");
                setStatus(STATUS_SEND_SUCCESS);
                if (VoiceRecordManager.this.sendMessageCallback != null) {
                    VoiceRecordManager.this.sendMessageCallback.onSuccess(message);
                }
            }

            @Override
            public void onError(io.rong.imlib.model.Message message, RongIMClient.ErrorCode errorCode) {
                Log.d(TAG, "onError() called with: message = [" + message + "], errorCode = [" + errorCode + "]");
                setStatus(STATUS_SEND_ERROR);
                if (VoiceRecordManager.this.sendMessageCallback != null) {
                    VoiceRecordManager.this.sendMessageCallback.onError(message, errorCode);
                }
            }
        };

        //发送普通消息
        RongIMClient.getInstance().sendMessage(conversationType,
                targetId, voiceMessage, null, null, sendMessageCallback);

    }

    private void startRecord(String outputFileName) {
        Log.d(TAG, "startRecord() called with: outputFileName = [" + outputFileName + "]");
        chatRecorder.exitRecorder();
        chatRecorder.initRecorder();
        chatRecorder.startRecord(outputFileName);
    }

    private long stopRecord() {
        Log.d(TAG, "stopRecord() called");
        return chatRecorder.stopRecord();
    }

    private void exitRecorder() {
        Log.d(TAG, "exitRecorder() called");
        chatRecorder.exitRecorder();
    }

    @Nullable
    public String getOutputFileName() {
        return chatRecorder.getOutputFileName();
    }

    public double getAmplitude() {
        return chatRecorder.getAmplitude();
    }

    public boolean isAutoSend() {
        return autoSend;
    }

    public void setAutoSend(boolean autoSend) {
        Log.d(TAG, "setAutoSend() called with: autoSend = [" + autoSend + "]");
        this.autoSend = autoSend;
    }

    public int getMinRecordDuration() {
        return minRecordDuration;
    }

    public void setMinRecordDuration(int minRecordDuration) {
        this.minRecordDuration = minRecordDuration;
    }

    public String getTargetId() {
        return targetId;
    }

    public Conversation.ConversationType getConversationType() {
        return conversationType;
    }

    public enum ErrorCode {
        RECORD_DURATION_TOO_SHORT(0, "录音时间太短"),
        OUTPUT_FILE_EMPTY(1, "目标文件为空");

        private int errorCode;
        private String detail;

        ErrorCode(int code, String detail) {
            this.errorCode = code;
            this.detail = detail;
        }

        public int getErrorCode() {
            return errorCode;
        }

        public String getDetail() {
            return detail;
        }
    }

    public interface onVoiceRecordListener {

        void onRecordStart(String path);

        void onRecordComplete(String path);

        void onRecordCancel(String path);

        void onRecordError(Throwable throwable, ErrorCode errorCode);
    }

}
