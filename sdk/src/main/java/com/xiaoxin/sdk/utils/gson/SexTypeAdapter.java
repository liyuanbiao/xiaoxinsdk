package com.xiaoxin.sdk.utils.gson;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.xiaoxin.sdk.data.Sex;

import java.io.IOException;

class SexTypeAdapter extends TypeAdapter<Sex> {
    @Override
    public void write(@NonNull JsonWriter out, @Nullable Sex value) throws IOException {
        if (value != null) {
            try {
                out.value(value.name());
            } catch (IOException e) {
                out.nullValue();
            }
        } else {
            out.nullValue();
        }
    }

    @Override
    @Nullable
    public Sex read(@NonNull JsonReader in) throws IOException {
        try {
            return Sex.valueOf(in.nextString());
        } catch (Exception e) {
            return null;
        }
    }
}
