package com.xiaoxin.sdk.utils;

import android.media.MediaRecorder;
import android.support.annotation.Nullable;

import com.xiaoxin.sdk.utils.Log;

import java.io.File;
import java.io.IOException;

public class ChatRecorder {
    private static final String TAG = "ChatRecorder";
    public static final int STATE_RECORD_NONE = 0;
    public static final int STATE_RECORD_INIT = 1;
    public static final int STATE_RECORD_START = 2;
    public static final int STATE_RECORD_ERROR = 3;
    public static final int STATE_RECORD_COMPLETE = 4;
    public static final int STATE_RECORD_RELEASE = 5;

    @Nullable
    private static ChatRecorder mChatRecorder = null;
    @Nullable
    private String mOutputFileName = null;
    @Nullable
    private MediaRecorder mRecorder = null;
    private long mStartTime = 0L;
    private int status;

    private OnStateChangeListener onStateChangeListener;

    public int getStatus() {
        return status;
    }

    private void setStatus(int status) {
        this.status = status;
        if (onStateChangeListener != null) {
            onStateChangeListener.onStateChange(status);
        }
    }

    public void setOnStateChangeListener(OnStateChangeListener onStateChangeListener) {
        this.onStateChangeListener = onStateChangeListener;
    }

    private ChatRecorder() {
        Log.d(TAG, "ChatRecorder() called");
    }

    @Nullable
    public static ChatRecorder getInstance() {
        if (mChatRecorder == null) {
            synchronized (ChatRecorder.class) {
                if (mChatRecorder == null) {
                    mChatRecorder = new ChatRecorder();
                }
            }
        }
        return mChatRecorder;
    }

    public static ChatRecorder newInstance() {
        return new ChatRecorder();
    }

    public void initRecorder() {
        Log.d(TAG, "initRecorder() called");
        mRecorder = new MediaRecorder();
        mStartTime = 0;
        setStatus(STATE_RECORD_INIT);
    }

    public void startRecord(String outputFileName) {
        Log.d(TAG, "startRecord() called with: outputFileName = [" + outputFileName + "]");
        if (mRecorder == null) {
            return;
        }
        this.mOutputFileName = outputFileName;
        File parentFile = new File(outputFileName).getParentFile();
        if (parentFile != null && (parentFile.exists() || parentFile.mkdirs())) {
            mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mRecorder.setOutputFormat(MediaRecorder.OutputFormat.AMR_NB);
            mRecorder.setOutputFile(mOutputFileName);
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            try {
                mRecorder.prepare();
            } catch (IOException e) {
                Log.e(TAG, "startRecord: ", e);
                mRecorder.release();
                setStatus(STATE_RECORD_ERROR);
                return;
            }
            setStatus(STATE_RECORD_START);
            mRecorder.start();
            mStartTime = System.currentTimeMillis();
        }
    }

    public long stopRecord() {
        Log.d(TAG, "stopRecord() called");
        if (mRecorder != null && mStartTime != 0) {
            long duration = 0;
            try {
                mRecorder.stop();
                duration = System.currentTimeMillis() - mStartTime;
                setStatus(STATE_RECORD_COMPLETE);
            } catch (IllegalStateException e) {
                Log.d(TAG, "stopRecord: ", e);
                mRecorder.release();
                setStatus(STATE_RECORD_ERROR);
            }
            return duration;
        }
        return 0L;
    }

    public void exitRecorder() {
        Log.d(TAG, "exitRecorder() called");
        if (mRecorder != null) {
            mRecorder.release();
            mRecorder = null;
            setStatus(STATE_RECORD_RELEASE);
        }
    }

    @Nullable
    public String getOutputFileName() {
        return mOutputFileName;
    }

    public double getAmplitude() {
        if (mRecorder != null) {
            return (mRecorder.getMaxAmplitude() / 2700.0);
        } else {
            return 0;
        }
    }

    public interface OnStateChangeListener {
        void onStateChange(int state);
    }
}