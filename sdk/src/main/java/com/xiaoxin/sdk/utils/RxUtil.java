package com.xiaoxin.sdk.utils;

import android.support.annotation.NonNull;

import io.reactivex.Completable;
import io.reactivex.CompletableSource;
import io.reactivex.CompletableTransformer;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.ObservableTransformer;
import io.reactivex.Single;
import io.reactivex.SingleSource;
import io.reactivex.SingleTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by ljm on 2017/4/27.
 */

public class RxUtil {

    public static <T> SchedulerTransformer<T> io_main() {
        return new SchedulerTransformer<>();
    }

    private static class SchedulerTransformer<U> implements
            ObservableTransformer<U, U>,
            SingleTransformer<U, U>, CompletableTransformer {

        @Override
        public ObservableSource<U> apply(@NonNull Observable<U> upstream) {
            return upstream.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());
        }

        @Override
        public SingleSource<U> apply(@NonNull Single<U> upstream) {
            return upstream.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());
        }

        @Override
        public CompletableSource apply(@NonNull Completable upstream) {
            return upstream.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());
        }
    }
}
