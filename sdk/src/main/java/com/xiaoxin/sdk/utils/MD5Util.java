package com.xiaoxin.sdk.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

class MD5Util {
    static String getMD5(String data) {
        try {
            MessageDigest digest = MessageDigest.getInstance("md5");
            byte[] results = digest.digest(data.getBytes());
            StringBuilder stringBuilder = new StringBuilder();
            for (byte result : results) {
                int i = result & 0xff;
                String str = Integer.toHexString(i);
                if (str.length() == 1) {
                    stringBuilder.append("0");
                }
                stringBuilder.append(str);
            }
            return stringBuilder.toString().toUpperCase();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }
}
