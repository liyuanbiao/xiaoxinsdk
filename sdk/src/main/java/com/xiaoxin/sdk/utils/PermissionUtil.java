package com.xiaoxin.sdk.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class PermissionUtil {
    private static final String TAG = "PermissionUtil";
    public static final int DEFAULT_REQUEST_PERMISSION_CODE = 0x100;

    public static final String[] AUDIO_CALL_PERMISSIONS =
            new String[]{Manifest.permission.RECORD_AUDIO};
    public static final String[] VIDEO_CALL_PERMISSIONS = new String[]{
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.CAMERA
    };

    /**
     * 请求语音电话所需权限
     *
     * @param activity    Activity
     * @param requestCode 请求码
     * @return 是否拥有权限
     */
    public static boolean requestAudioCallPermissions(@NonNull Activity activity, int requestCode) {
        return requestPermissions(activity, AUDIO_CALL_PERMISSIONS, requestCode);
    }

    /**
     * 请求视频电话所需权限
     *
     * @param activity    Activity
     * @param requestCode 请求码
     * @return 是否拥有权限
     */
    public static boolean requestVideoCallPermissions(@NonNull Activity activity, int requestCode) {
        return requestPermissions(activity, VIDEO_CALL_PERMISSIONS, requestCode);
    }

    /**
     * 检查音频电话所需权限
     *
     * @param context Context
     * @return 是否拥有权限
     */
    public static boolean hasAudioCallPermission(@NonNull Context context) {
        boolean granted = true;
        for (String permission : AUDIO_CALL_PERMISSIONS) {
            granted &= hasPermission(context, permission);
        }
        if (!granted && Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            granted = hasRecordPermission(context);
        }
        return granted;
    }

    /**
     * 检查视频电话所需权限
     *
     * @param context Context
     * @return 是否拥有权限
     */
    public static boolean hasVideoCallPermission(@NonNull Context context) {
        boolean granted = true;
        for (String permission : VIDEO_CALL_PERMISSIONS) {
            granted &= hasPermission(context, permission);
        }
        return granted;
    }

    /**
     * 请求权限
     *
     * @param activity    Activity
     * @param permissions 需要请求的权限
     * @param requestCode 请求码
     * @return 是否已经全部拥有权限
     */
    public static boolean requestPermissions(
            @NonNull Activity activity,
            @NonNull String[] permissions, int requestCode) {
        Log.d(TAG, "requestPermissions() called with: " +
                "\nactivity = [" + activity + "]," +
                "\npermissions = [" + Arrays.toString(permissions) + "], " +
                "\nrequestCode = [" + requestCode + "]");
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        activity = Objects.requireNonNull(activity, "activity must not be null");

        List<String> deniedPermissions = new ArrayList<>();
        for (String permission : permissions) {
            if (!hasPermission(activity, permission)) {
                deniedPermissions.add(permission);
            }
        }
        if (deniedPermissions.size() > 0) {
            ActivityCompat.requestPermissions(activity,
                    deniedPermissions.toArray(new String[0]), requestCode);
            return false;
        }
        return true;
    }

    /**
     * 请求权限
     *
     * @param activity    Activity
     * @param permissions 需要请求的权限
     * @return 是否已经全部拥有权限
     */
    public static boolean requestPermissions(
            @NonNull Activity activity,
            @NonNull String[] permissions) {
        return requestPermissions(activity, permissions, DEFAULT_REQUEST_PERMISSION_CODE);
    }

    /**
     * 检查是否拥有某权限
     *
     * @param context    Context
     * @param permission 需要检查的权限
     * @return true or false
     */
    public static boolean hasPermission(
            @NonNull Context context,
            @NonNull String permission) {
        return checkPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
    }

    private static int checkPermission(
            @NonNull Context context,
            @NonNull String permission) {
        context = Objects.requireNonNull(context, "Context must not be null");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return context.checkSelfPermission(permission);
        } else {
            return context.getPackageManager()
                    .checkPermission(permission, context.getPackageName());
        }
    }

    private static boolean hasRecordPermission(@NonNull Context context) {
        boolean hasPermission = false;
        int bufferSizeInBytes = AudioRecord.getMinBufferSize(44100,
                12, AudioFormat.ENCODING_PCM_16BIT);
        AudioRecord audioRecord = null;
        try {
            audioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC,
                    44100, 12, AudioFormat.ENCODING_PCM_16BIT, bufferSizeInBytes);
            audioRecord.startRecording();
            if (audioRecord.getRecordingState() ==
                    AudioRecord.RECORDSTATE_RECORDING) {
                audioRecord.stop();
                hasPermission = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            hasPermission = false;
        } finally {
            if (audioRecord != null) {
                audioRecord.release();
            }
        }
        return hasPermission;
    }
}
