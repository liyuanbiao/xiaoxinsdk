package com.xiaoxin.sdk.utils;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class Log {
    public static final int VERBOSE = 2;
    public static final int DEBUG = 3;
    public static final int INFO = 4;
    public static final int WARN = 5;
    public static final int ERROR = 6;
    public static final int ASSERT = 7;

    private static final String TAG = "XiaoXinSDK";
    private static boolean debug = true;

    private Log() {
    }

    public static void setDebug(boolean debug) {
        d(TAG, "setDebug() called with: debug = [" + debug + "]");
        Log.debug = debug;
    }

    public static boolean isDebug() {
        return debug;
    }

    public static int v(@NonNull String tag, @Nullable String msg) {
        if (!debug) return -1;
        return android.util.Log.v(TAG, "[ " + tag + " ] " + msg);
    }

    public static int d(@NonNull String tag, @Nullable String msg) {
        if (!debug) return -1;
        return android.util.Log.d(TAG, "[ " + tag + " ] " + msg);
    }

    public static int d(@NonNull String tag, @Nullable String msg, @Nullable Throwable throwable) {
        if (!debug) return -1;
        return android.util.Log.d(TAG, "[ " + tag + " ] " + msg, throwable);
    }

    public static int i(@NonNull String tag, @Nullable String msg) {
        if (!debug) return -1;
        return android.util.Log.i(TAG, "[ " + tag + " ] " + msg);
    }

    public static int w(@NonNull String tag, @Nullable String msg) {
        if (!debug) return -1;
        return android.util.Log.w(TAG, "[ " + tag + " ] " + msg);
    }

    public static int e(@NonNull String tag, @Nullable String msg) {
        if (!debug) return -1;
        return android.util.Log.e(TAG, "[ " + tag + " ] " + msg);
    }

    public static int e(@NonNull String tag, @Nullable String msg, @Nullable Throwable throwable) {
        if (!debug) return -1;
        return android.util.Log.e(TAG, "[ " + tag + " ] " + msg, throwable);
    }

    public static int println(int priority, @Nullable String tag, @Nullable String msg) {
        if (!debug) return -1;
        return android.util.Log.println(priority, TAG, "[ " + tag + " ] " + msg);
    }
}
