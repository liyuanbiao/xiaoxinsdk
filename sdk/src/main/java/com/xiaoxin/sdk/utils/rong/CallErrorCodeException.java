package com.xiaoxin.sdk.utils.rong;

import android.support.annotation.NonNull;

import io.rong.calllib.RongCallCommon;

public class CallErrorCodeException extends Exception {
    private RongCallCommon.CallErrorCode callErrorCode;

    public CallErrorCodeException(RongCallCommon.CallErrorCode callErrorCode) {
        this.callErrorCode = callErrorCode;
    }

    public RongCallCommon.CallErrorCode getCallErrorCode() {
        return callErrorCode;
    }

    @NonNull
    @Override
    public String toString() {
        return "CallErrorCodeException{" +
                "callErrorCode=" + callErrorCode +
                '}';
    }
}
