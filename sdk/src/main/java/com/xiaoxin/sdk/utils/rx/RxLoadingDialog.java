package com.xiaoxin.sdk.utils.rx;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;

import com.xiaoxin.sdk.view.LoadingDialog;

import java.util.concurrent.Executor;

import io.reactivex.Single;
import io.reactivex.SingleSource;
import io.reactivex.SingleTransformer;

public class RxLoadingDialog {
    private static final Executor MAIN_EXECUTOR = new Executor() {
        private final Handler HANDLER = new Handler(Looper.getMainLooper());

        @Override
        public void execute(Runnable command) {
            HANDLER.post(command);
        }
    };

    public static <U> SingleTransformer<U, U> loading(@NonNull Context context) {
        return new SingleTransformer<U, U>() {
            @NonNull
            private LoadingDialog dialog = LoadingDialog.instanceNS(context);

            @Override
            public SingleSource<U> apply(@NonNull Single<U> upstream) {
                return upstream.doOnSubscribe(disposable ->
                        MAIN_EXECUTOR.execute(() -> {
                            final Context dialogContext = dialog.getContext();
                            if (dialogContext instanceof Activity) {
                                if (!((Activity) dialogContext).isDestroyed() &&
                                        !dialog.isShowing()) {
                                    dialog.show();
                                }
                            } else {
                                if (!dialog.isShowing()) {
                                    dialog.show();
                                }
                            }
                        })).doFinally(() ->
                        MAIN_EXECUTOR.execute(() -> {
                            if (dialog.isShowing()) {
                                dialog.dismiss();
                            }
                        }));
            }
        };
    }
}
