package com.xiaoxin.sdk.utils.rong;

import com.xiaoxin.sdk.data.Person;
import com.xiaoxin.sdk.internal.rong.User;

import java.util.List;

import io.rong.imkit.RongIM;
import io.rong.imlib.model.UserInfo;

public class RongIMUtil {
    public static void setCurrentUserInfo(Person person) {
        RongIM.getInstance().setCurrentUserInfo(person.toUserInfo());
    }

    public static void refreshUserInfoCache(UserInfo userInfo) {
        RongIM.getInstance().refreshUserInfoCache(userInfo);
    }

    public static void refreshUserInfoCache(User user) {
        RongIM.getInstance().refreshUserInfoCache(user.toUserInfo());
    }

    public static void refreshUserInfoCache(Person person) {
        RongIM.getInstance().refreshUserInfoCache(person.toUserInfo());
    }

    public static void refreshUserInfoCache(List<Person> list) {
        for (Person person : list) {
            refreshUserInfoCache(person);
        }
    }
}
