package com.xiaoxin.sdk.utils.rong;

import android.support.annotation.NonNull;
import android.view.SurfaceView;

import com.xiaoxin.sdk.utils.Log;

import io.rong.calllib.IRongCallListener;
import io.rong.calllib.RongCallCommon;
import io.rong.calllib.RongCallSession;

public class BaseRongCallListener implements IRongCallListener {
    private static final String TAG = "BaseRongCallListener";

    @Override
    public void onCallOutgoing(@NonNull RongCallSession rongCallSession, SurfaceView surfaceView) {
        Log.d(TAG, "onCallOutgoing() called with: rongCallSession = [" + rongCallSession.getCallId() + "], surfaceView = [" + surfaceView + "]");
    }

    @Override
    public void onCallConnected(@NonNull RongCallSession rongCallSession, SurfaceView surfaceView) {
        Log.d(TAG, "onCallConnected() called with: rongCallSession = [" + rongCallSession.getCallId() + "], surfaceView = [" + surfaceView + "]");
    }

    @Override
    public void onCallDisconnected(@NonNull RongCallSession rongCallSession, RongCallCommon.CallDisconnectedReason callDisconnectedReason) {
        Log.d(TAG, "onCallDisconnected() called with: rongCallSession = [" + rongCallSession.getCallId() + "], callDisconnectedReason = [" + callDisconnectedReason + "]");
    }

    @Override
    public void onRemoteUserRinging(String userId) {
        Log.d(TAG, "onRemoteUserRinging() called with: userId = [" + userId + "]");
    }

    @Override
    public void onRemoteUserJoined(String userId, RongCallCommon.CallMediaType callMediaType, SurfaceView surfaceView) {
        Log.d(TAG, "onRemoteUserJoined() called with: userId = [" + userId + "], callMediaType = [" + callMediaType + "], surfaceView = [" + surfaceView + "]");
    }

    @Override
    public void onRemoteUserInvited(String userId, RongCallCommon.CallMediaType callMediaType) {
        Log.d(TAG, "onRemoteUserInvited() called with: userId = [" + userId + "], callMediaType = [" + callMediaType + "]");

    }

    @Override
    public void onRemoteUserLeft(String userId, RongCallCommon.CallDisconnectedReason callDisconnectedReason) {
        Log.d(TAG, "onRemoteUserLeft() called with: userId = [" + userId + "], callDisconnectedReason = [" + callDisconnectedReason + "]");
    }

    @Override
    public void onMediaTypeChanged(String userId, RongCallCommon.CallMediaType callMediaType, SurfaceView surfaceView) {
        Log.d(TAG, "onMediaTypeChanged() called with: userId = [" + userId + "], callMediaType = [" + callMediaType + "], surfaceView = [" + surfaceView + "]");
    }

    @Override
    public void onError(RongCallCommon.CallErrorCode callErrorCode) {
        Log.d(TAG, "onError() called with: callErrorCode = [" + callErrorCode + "]");
    }

    @Override
    public void onRemoteCameraDisabled(String userId, boolean disabled) {
        Log.d(TAG, "onRemoteCameraDisabled() called with: userId = [" + userId + "], disabled = [" + disabled + "]");
    }
}
