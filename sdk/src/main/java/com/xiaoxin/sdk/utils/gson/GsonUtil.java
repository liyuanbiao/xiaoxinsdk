package com.xiaoxin.sdk.utils.gson;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.xiaoxin.sdk.data.SOSRecord;
import com.xiaoxin.sdk.data.Sex;

import java.util.Date;

public class GsonUtil {
    private GsonUtil() {
    }

    public static Gson getDefault() {
        return new GsonBuilder()
                .registerTypeAdapter(Date.class, new DateSerializer())
                .registerTypeAdapter(Sex.class, new SexTypeAdapter())
                .registerTypeAdapter(SOSRecord.Status.class, new StatusTypeAdapter())
                .create();
    }
}
