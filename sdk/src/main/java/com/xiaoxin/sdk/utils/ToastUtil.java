package com.xiaoxin.sdk.utils;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.xiaoxin.sdk.Xiaoxin;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.concurrent.Executor;

public final class ToastUtil {
    private ToastUtil() {
        throw new IllegalAccessError("ToastUtil is Util class");
    }

    @NonNull
    private static final Executor mainExecutor = new Executor() {
        @NonNull
        private Handler handler = new Handler(Looper.getMainLooper());

        @Override
        public void execute(Runnable command) {
            handler.post(command);
        }
    };

    public static void show(String text) {
        show(text, Toast.LENGTH_SHORT);
    }

    @IntDef(value = {Toast.LENGTH_SHORT, Toast.LENGTH_LONG})
    @Retention(RetentionPolicy.SOURCE)
    @interface Duration {
    }

    public static void show(String text, @Duration int duration) {
        mainExecutor.execute(() -> Toast.makeText(Xiaoxin.getContext(), text, duration).show());
    }
}
