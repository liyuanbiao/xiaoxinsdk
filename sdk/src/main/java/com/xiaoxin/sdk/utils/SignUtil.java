package com.xiaoxin.sdk.utils;


import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class SignUtil {
    private static String PRIVATE_KEY;

    public static void init(@NonNull String privateKey) {
        PRIVATE_KEY = privateKey;
    }

    @Nullable
    public static String sign(@NonNull String data) {
        try {
            String md5 = MD5Util.getMD5(data);
            return RSAUtil.encrypt(md5.getBytes(),
                    RSAUtil.getPrivateKey(PRIVATE_KEY));
        } catch (Exception e) {
            return null;
        }
    }

}
