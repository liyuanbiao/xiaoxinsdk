package com.xiaoxin.sdk.utils.rong;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import io.rong.calllib.RongCallCommon;

public class CallDisconnectedException extends Exception {
    @NonNull
    private RongCallCommon.CallDisconnectedReason callDisconnectedReason;

    public CallDisconnectedException(@NonNull RongCallCommon.CallDisconnectedReason callDisconnectedReason) {
        this.callDisconnectedReason = callDisconnectedReason;
    }

    public CallDisconnectedException(
            @NonNull RongCallCommon.CallDisconnectedReason callDisconnectedReason,
            @Nullable String message) {
        super(message);
        this.callDisconnectedReason = callDisconnectedReason;
    }

    @NonNull
    public RongCallCommon.CallDisconnectedReason getCallDisconnectedReason() {
        return callDisconnectedReason;
    }

    @NonNull
    @Override
    public String toString() {
        return "CallDisconnectedException{" +
                "callDisconnectedReason=" + callDisconnectedReason + "," +
                "message=" + getMessage() +
                '}';
    }
}
