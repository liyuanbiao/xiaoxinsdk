package com.xiaoxin.sdk.utils;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.imageaware.ImageAware;
import com.nostra13.universalimageloader.core.imageaware.ImageViewAware;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.xiaoxin.sdk.http.Api;


public class ImageLoader {
    public static void displayImage(@NonNull String uri, @NonNull ImageView imageView) {
        displayImage(uri, new ImageViewAware(imageView), null, null, null);
    }

    private static void displayImage(@NonNull String uri, @NonNull ImageAware imageAware,
                                     @Nullable DisplayImageOptions options,
                                     @Nullable ImageLoadingListener listener,
                                     @Nullable ImageLoadingProgressListener progressListener) {
        //处理一下uri
        uri = parseUri(uri);
        com.nostra13.universalimageloader.core.ImageLoader.getInstance()
                .displayImage(uri, imageAware, options, listener, progressListener);
    }

    @Nullable
    private static String parseUri(@Nullable String uri) {
        if (uri == null) {
            return null;
        }
        if (uri.startsWith("http://") ||
                uri.startsWith("https://") ||
                uri.startsWith("file://") ||
                uri.startsWith("content://") ||
                uri.startsWith("assets://") ||
                uri.startsWith("drawable://")) {
            return uri;
        } else {
            return Api.IMAGE_PATH + uri;
        }
    }

}
