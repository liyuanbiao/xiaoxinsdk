package com.xiaoxin.sdk.utils;

import android.support.annotation.Nullable;

import com.xiaoxin.rximlib.ErrorCodeException;
import com.xiaoxin.rximlib.TokenIncorrectException;
import com.xiaoxin.sdk.utils.rong.CallDisconnectedException;
import com.xiaoxin.sdk.utils.rong.CallErrorCodeException;

import io.reactivex.functions.Consumer;
import io.reactivex.plugins.RxJavaPlugins;

public final class ErrorHandler {
    private static boolean toastOnError = false;

    private ErrorHandler() {
        throw new IllegalAccessError("ErrorHandler is Util class");
    }

    /**
     * 设置是否在错误时显示 toast
     *
     * @param toastOnError true or false
     */
    public static void setToastOnError(boolean toastOnError) {
        ErrorHandler.toastOnError = toastOnError;
        Consumer<? super Throwable> errorHandler = RxJavaPlugins.getErrorHandler();
        if (!toastOnError) {
            if (errorHandler instanceof ErrorHandlerConsumer) {
                Consumer<? super Throwable> source = ((ErrorHandlerConsumer) errorHandler).getSource();
                RxJavaPlugins.setErrorHandler(source);
            }
        } else {
            if (!(errorHandler instanceof ErrorHandlerConsumer)) {
                RxJavaPlugins.setErrorHandler(new ErrorHandlerConsumer(errorHandler));
            }
        }
    }

    private static class ErrorHandlerConsumer implements Consumer<Throwable> {
        @Nullable
        private Consumer<? super Throwable> source;

        public void setSource(@Nullable Consumer<? super Throwable> source) {
            this.source = source;
        }

        @Nullable
        public Consumer<? super Throwable> getSource() {
            return source;
        }

        public ErrorHandlerConsumer(@Nullable Consumer<? super Throwable> source) {
            this.source = source;
        }

        @Override
        public void accept(Throwable throwable) throws Exception {
            if (throwable instanceof ErrorCodeException || //错误码
                    throwable instanceof TokenIncorrectException || //Token失效
                    throwable instanceof CallDisconnectedException || //通话断开
                    throwable instanceof CallErrorCodeException) { //通话错误码
                ToastUtil.show(throwable.toString());
            }
            if (source != null) {
                source.accept(throwable);
            }
        }
    }

    /**
     * 捕获异常
     *
     * @param throwable 异常
     */
    public static void onError(Throwable throwable) {
        Consumer<? super Throwable> consumer = getErrorHandler();
        if (consumer != null) {
            try {
                consumer.accept(throwable);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 是否在错误时显示toast
     *
     * @return true or false
     */
    public static boolean isToastOnError() {
        return toastOnError;
    }

    /**
     * 设置默认的错误处理方式
     *
     * @param handler 默认的错误处理方式
     */
    public static void setErrorHandler(Consumer<? super Throwable> handler) {
        RxJavaPlugins.setErrorHandler(handler);
    }

    /**
     * 获取默认的错误处理方式
     *
     * @return Consumer
     */
    @Nullable
    public static Consumer<? super Throwable> getErrorHandler() {
        return RxJavaPlugins.getErrorHandler();
    }
}
