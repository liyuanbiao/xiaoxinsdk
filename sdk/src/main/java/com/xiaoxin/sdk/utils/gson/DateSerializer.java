package com.xiaoxin.sdk.utils.gson;

import android.support.annotation.NonNull;

import com.xiaoxin.sdk.utils.Log;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

class DateSerializer implements JsonDeserializer<Date>, JsonSerializer<Date> {
    private static final String TAG = "DateSerializer";
    //2016-07-27T15:28:55.130Z
    private static final String[] DATE_FORMATS = new String[]{
            "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
            "yyyy-MM-dd HH:mm:ss",
            "yyyy/MM/dd HH:mm:ss",
            "yyyy-MM-dd",
            "yyyy-MM-dd'T'HH:mm:ss",
            "yyyy-MM-dd'T'HH:mm:ssZ"
    };
    @NonNull
    private static SimpleDateFormat dateFormat =
            new SimpleDateFormat(DATE_FORMATS[0], Locale.getDefault());

    @NonNull
    public static SimpleDateFormat getDateFormat() {
        return dateFormat;
    }

    static {
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
    }

    @Override
    public Date deserialize(@NonNull JsonElement jsonElement, Type typeOF,
                            JsonDeserializationContext context) throws JsonParseException {
        Log.d(TAG, "deserialize() called");
        String date = jsonElement.getAsString();
        for (String format : DATE_FORMATS) {
            try {
                SimpleDateFormat dateFormat = new SimpleDateFormat(format, Locale.getDefault());
                dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                return dateFormat.parse(date);
            } catch (ParseException e) {
                Log.e(TAG, "deserialize: ", e);
            }
        }
        throw new JsonParseException("Unparseable date: \"" + date + "\". Supported formats: "
                + Arrays.toString(DATE_FORMATS));
    }

    @NonNull
    @Override
    public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext context) {
        return new JsonPrimitive(dateFormat.format(src));
    }

}
