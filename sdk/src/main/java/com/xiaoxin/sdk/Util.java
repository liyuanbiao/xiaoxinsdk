package com.xiaoxin.sdk;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresPermission;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Base64;

import com.iflytek.cloud.Setting;
import com.iflytek.cloud.SpeechUtility;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.xiaoxin.sdk.utils.Log;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.UUID;

import io.rong.calllib.RongCallClient;
import io.rong.calllib.RongCallSession;

final class Util {
    private static final String TAG = "Util";

    private Util() {
        throw new IllegalAccessError("Util is Util class");
    }

    /**
     * 获取设备唯一标识
     *
     * @param context Context
     * @return 设备唯一标识 Uniqueid
     */
    @NonNull
    @SuppressLint("MissingPermission")
    @RequiresPermission(android.Manifest.permission.READ_PHONE_STATE)
    static String getUniqueid(@NonNull Context context) {
        String uuid = getImei(context);
        if (TextUtils.isEmpty(uuid)) {
            uuid = getAndroidId(context);
        }
        if (TextUtils.isEmpty(uuid)) {
            uuid = getMacAddress(context);
        }
        if (uuid != null) {
            uuid = uuid.replace(":", "");
        }
        if (uuid == null || uuid.contains("00000")) {
            uuid = UUID.randomUUID().toString().replace("-", "");
        }
        Log.d(TAG, "getUniqueid() returned: " + uuid);
        return uuid;
    }

    /**
     * 获取设备IMEI，如果IMEI不存在则获取MEID
     *
     * @param context Context
     * @return IMEI or MEID
     */
    @Nullable
    @SuppressLint({"HardwareIds", "MissingPermission"})
    @RequiresPermission(android.Manifest.permission.READ_PHONE_STATE)
    private static String getImei(@NonNull Context context) {
        TelephonyManager tm = (TelephonyManager) context.getApplicationContext()
                .getSystemService(Context.TELEPHONY_SERVICE);
        if (tm == null) return null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            return tm.getImei();
        } else {
            return tm.getDeviceId();
        }
    }

    /**
     * 获取MacAddress
     *
     * @param context Context
     * @return MacAddress
     */
    @Nullable
    @SuppressLint("HardwareIds")
    private static String getMacAddress(@NonNull Context context) {
        WifiManager wifiManager = (WifiManager) context.getApplicationContext()
                .getSystemService(Context.WIFI_SERVICE);
        if (wifiManager == null) return null;
        WifiInfo info = wifiManager.getConnectionInfo();
        return info.getMacAddress();
    }

    /**
     * Android Id
     *
     * @param context Context
     * @return Android Id
     */
    @SuppressLint("HardwareIds")
    private static String getAndroidId(@NonNull Context context) {
        ContentResolver contentResolver = context.getApplicationContext().getContentResolver();
        return Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID);
    }

    /**
     * 获得当前进程的名字
     *
     * @param context Context
     * @return 当前进程的名字
     */
    @Nullable
    static String getCurProcessName(@NonNull Context context) {
        int pid = android.os.Process.myPid();
        ActivityManager activityManager = (ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningAppProcessInfo appProcess : activityManager
                .getRunningAppProcesses()) {
            if (appProcess.pid == pid) {
                return appProcess.processName;
            }
        }
        return null;
    }

    /**
     * 获取融云AppKey
     *
     * @return 融云AppKey
     */
    static String getRongCloudAppKey() {
        String sign = "90-84-86-48-78-71-57-49-100-110-66-48-100-71-100-51-89-81-61-61-";
        String[] dfs = sign.split("-");
        byte[] asd = new byte[dfs.length];
        for (int i = 0; i < dfs.length; i++) {
            asd[i] = Byte.parseByte(dfs[i]);
        }
        return new String(Base64.decode(asd, Base64.DEFAULT));
    }


    /**
     * 是否在VOIP通话中
     *
     * @param context Context
     * @return 是否在VOIP通话中
     */
    static boolean isInVoipCall(@NonNull Context context) {
        RongCallSession callSession = RongCallClient.getInstance().getCallSession();
        return callSession != null && callSession.getActiveTime() > 0;
    }

    /**
     * 初始化图片加载框架
     *
     * @param context Context
     */
    static void initImageLoader(@NonNull Context context) {
        context = context.getApplicationContext();
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageOnFail(R.drawable.ic_user_head)
                .showImageForEmptyUri(R.drawable.ic_user_head)
                .showImageOnLoading(R.drawable.ic_user_head)
                .resetViewBeforeLoading(true) // default
                .cacheInMemory(true) // default
                .cacheOnDisk(true)
                .build();

        ImageLoaderConfiguration configuration = new ImageLoaderConfiguration.Builder(context)
                .imageDownloader(new BaseImageDownloader(context) {
                    @Override
                    protected HttpURLConnection createConnection(String url,
                                                                 Object extra) throws IOException {
                        HttpURLConnection conn = super.createConnection(url, extra);
                        conn.setRequestProperty("User-agent", "Mozilla/4.0");
                        return conn;
                    }
                }).defaultDisplayImageOptions(options)
                .build();
        ImageLoader.getInstance().init(configuration);
    }

    /**
     * 初始化讯飞TTS
     *
     * @param context Context
     */
    static void initTts(Context context) {
        Log.d(TAG, "initTts() called");
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), PackageManager.GET_META_DATA);
            if (packageInfo != null) {
                Bundle metaData = packageInfo.applicationInfo.metaData;
                if (metaData != null) {
                    String appId = metaData.getString("IFLYTEK_APPKEY");
                    if (!TextUtils.isEmpty(appId)) {
                        SpeechUtility.createUtility(context, String.format("appid=%s", appId));
                    }
                }
            }
            Setting.setShowLog(Xiaoxin.isDebug());
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }
}
