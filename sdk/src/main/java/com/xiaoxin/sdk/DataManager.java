package com.xiaoxin.sdk;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.xiaoxin.sdk.call.OnCallXiaoxin;
import com.xiaoxin.sdk.data.Person;
import com.xiaoxin.sdk.utils.Log;

import io.rong.imlib.RongIMClient;


/**
 * Created by zhangzhi on 16/7/28.
 */
public class DataManager {

    public static final String TAG = "DataManager";
    private static final String TOKEN = "token";
    private static final String RONG_TOKEN = "rong_token";
    @SuppressLint("StaticFieldLeak")
    private static DataRepository dataRepository;

    static void init(@NonNull Context context,
                     @NonNull String uniqueid,
                     @NonNull String appKey) {
        dataRepository = new DataRepository(context, uniqueid, appKey);
    }

    public static DataRepository getDataRepository() {
        return dataRepository;
    }

    @Nullable
    public static String getToken() {
        Person person = getPerson();
        return person != null ? person.getToken() : "";
    }

    @Nullable
    public static String getRongToken() {
        Person person = getPerson();
        return person != null ? person.getRongToken() : "";
    }

    @Nullable
    public static Person getPerson() {
        return dataRepository.getLocalPerson(dataRepository.getUniqueid());
    }

    static void login(OnCallXiaoxin onCallXiaoxin) {
        Log.d(TAG, "login() called");
        connectRong(onCallXiaoxin);
    }

    static void login() {
        Log.d(TAG, "login() called");
        connectRong(null);
    }

    private static void connectRong(final OnCallXiaoxin onCallXiaoxin) {
        Log.d(TAG, "connectRong() called");
        dataRepository.setOnCallXiaoxin(onCallXiaoxin);
        dataRepository.connectRongCloud();
    }

    public static void logout() {
        Log.d(TAG, "logout() called");
        //清除token
        dataRepository.logout();
        RongIMClient.getInstance().logout();
    }

}
