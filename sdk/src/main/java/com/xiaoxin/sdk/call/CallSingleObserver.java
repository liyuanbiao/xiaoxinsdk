package com.xiaoxin.sdk.call;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;

public class CallSingleObserver<T> implements SingleObserver<T>, OnCallListener<T> {
    @Nullable
    private OnCallListener<T> onCallListener;

    public CallSingleObserver(@Nullable OnCallListener<T> onCallListener) {
        this.onCallListener = onCallListener;
    }

    public CallSingleObserver() {
    }

    @Override
    public void onSubscribe(Disposable d) {

    }

    @Override
    public void onSuccess(@NonNull T t) {
        if (onCallListener != null) {
            onCallListener.onSuccess(t);
        }
    }

    @Override
    public void onError(@NonNull Throwable e) {
        if (onCallListener != null) {
            onCallListener.onError(e);
        }
    }
}
