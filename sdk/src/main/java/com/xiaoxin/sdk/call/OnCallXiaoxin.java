package com.xiaoxin.sdk.call;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.xiaoxin.sdk.data.Person;

import io.rong.imlib.RongIMClient;


public interface OnCallXiaoxin {
    /**
     * 初始化
     */
    void init();

    /**
     * 获取Person成功
     *
     * @param data Person
     */
    void onSuccessXX(@NonNull Person data);

    /**
     * 获取Person失败
     *
     * @param throwable 错误
     */
    void onErrorXX(@Nullable Throwable throwable);

    /**
     * 连接融云成功
     *
     * @param data UserId
     */
    void onSuccessRG(String data);

    /**
     * 连接融云失败
     *
     * @param errorCode 错误码
     */
    void onErrorRG(@Nullable RongIMClient.ErrorCode errorCode);
}
