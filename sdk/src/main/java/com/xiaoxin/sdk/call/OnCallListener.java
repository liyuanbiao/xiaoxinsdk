package com.xiaoxin.sdk.call;

import android.support.annotation.NonNull;

/**
 * Created by ljman on 2018/4/10.
 */

public interface OnCallListener<T> {
    void onSuccess(@NonNull T t);

    void onError(@NonNull Throwable throwable);
}
