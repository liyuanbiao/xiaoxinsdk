package com.xiaoxin.sdk;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.xiaoxin.rximlib.ErrorCodeException;
import com.xiaoxin.rximlib.RxRongIMClient;
import com.xiaoxin.rximlib.TokenIncorrectException;
import com.xiaoxin.sdk.call.OnCallXiaoxin;
import com.xiaoxin.sdk.data.Person;
import com.xiaoxin.sdk.http.Api;
import com.xiaoxin.sdk.utils.Log;
import com.xiaoxin.sdk.utils.gson.GsonUtil;
import com.xiaoxin.sdk.utils.rong.RongIMUtil;

import java.util.Objects;

import io.reactivex.Maybe;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

class DataRepository {
    private static final String TAG = "DataRepository";
    private static final String USER_PREFERENCES = "user_preferences";
    private static final String KEY_TOKEN = "token";
    private static final String KEY_RONG_TOKEN = "rongToken";
    @NonNull
    private Context context;
    @NonNull
    private String uniqueid;
    @NonNull
    private String appKey;
    private SharedPreferences user;
    @Nullable
    private OnCallXiaoxin onCallXiaoxin;

    void setOnCallXiaoxin(@Nullable OnCallXiaoxin onCallXiaoxin) {
        Log.d(TAG, "setOnCallXiaoxin() called with: onCallXiaoxin = [" + onCallXiaoxin + "]");
        this.onCallXiaoxin = onCallXiaoxin;
    }

    /**
     * @param uniqueid 设备唯一标志
     * @param appKey   融云AppKey
     */
    DataRepository(@NonNull Context context,
                   @NonNull String uniqueid,
                   @NonNull String appKey) {
        this.context = Objects.requireNonNull(context, "Context must not be null");
        this.uniqueid = Objects.requireNonNull(uniqueid, "uniqueid must not be null");
        this.appKey = Objects.requireNonNull(appKey, "AppKey must not be null");
        user = this.context.getSharedPreferences(USER_PREFERENCES, Context.MODE_PRIVATE);
    }

    @NonNull
    public String getUniqueid() {
        return uniqueid;
    }

    @NonNull
    public String getAppKey() {
        return appKey;
    }


    public Single<Person> getPersonByUniqueID() {
        return getPersonByUniqueID(appKey, uniqueid);
    }

    /**
     * 获取Person
     *
     * @param uniqueid 设备唯一标志
     * @param appkey   融云AppKey
     * @return Person
     */
    public Single<Person> getPersonByUniqueID(@NonNull String appkey, @NonNull String uniqueid) {
        Single<Person> personSingle = getPersonByUniqueIDFromNet(appkey, uniqueid);
        Maybe<Person> personMaybe = getPersonByUniqueIDFromLocal(uniqueid);
        return personMaybe.toSingle().onErrorResumeNext(personSingle);
    }

    @NonNull
    public Maybe<Person> getPersonByUniqueIDFromLocal(@NonNull String uniqueid) {
        return Maybe.fromCallable(() -> getLocalPerson(uniqueid))
                .subscribeOn(Schedulers.io());
    }

    @Nullable
    Person getLocalPerson(@NonNull String uniqueid) {
        String value = user.getString(uniqueid, null);
        if (!TextUtils.isEmpty(value)) {
            return GsonUtil.getDefault().fromJson(value, Person.class);
        }
        return null;
    }

    @NonNull
    public Single<Person> getPersonByUniqueIDFromNet(@NonNull String appkey,
                                                     @NonNull String uniqueid) {
        return Api.service().getPerson(uniqueid, appkey)
                .doOnSuccess(person -> {
                    String value = GsonUtil.getDefault().toJson(person);
                    user.edit().putString(uniqueid, value).apply();
                });
    }

    /**
     * 获取RongToken
     *
     * @return RongToken
     */
    @NonNull
    public Single<String> getRongToken() {
        return getPersonByUniqueID(appKey, uniqueid).map(Person::getRongToken);
    }

    /**
     * 获取Token
     *
     * @return Token
     */
    @NonNull
    public Single<String> getToken() {
        return getPersonByUniqueID(appKey, uniqueid).map(Person::getToken);
    }

    /**
     * 登出
     */
    void logout() {
        Log.d(TAG, "logout() called");
        user.edit().remove(getUniqueid()).apply();
    }

    /**
     * 连接融云服务器，自动处理token过期的情况
     * 网络失败的情况SDK自己做了处理，所以这里不做处理
     */
    public void connectRongCloud() {
        Log.d(TAG, "connectRongCloud() called");
        getPersonInvokeCallback()
                .map(Person::getRongToken)
                .flatMap(RxRongIMClient::connect)
                .onErrorResumeNext(throwable -> {
                    //如果是token过期则需要重新请求
                    if (throwable instanceof TokenIncorrectException) {
                        return getPersonByUniqueIDFromNet(appKey, uniqueid)
                                .map(Person::getRongToken)
                                .flatMap(RxRongIMClient::connect);
                    } else {
                        return Single.error(throwable);
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<String>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.d(TAG, "onSubscribe() called with: d = [" + d + "]");
                    }

                    @Override
                    public void onSuccess(String userId) {
                        Log.d(TAG, "onSuccess() called with: userId = [" + userId + "]");
                        if (onCallXiaoxin != null) {
                            onCallXiaoxin.onSuccessRG(userId);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError() called with: e = [" + e + "]");
                        if (onCallXiaoxin != null &&
                                e instanceof ErrorCodeException) {
                            onCallXiaoxin.onErrorRG(((ErrorCodeException) e).getErrorCode());
                        }
                    }
                });
    }

    private Single<Person> getPersonInvokeCallback() {
        return getPersonByUniqueID(appKey, uniqueid)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess(person -> {
                    RongIMUtil.setCurrentUserInfo(person);
                    Log.d(TAG, "getPerson doOnSuccess() called with person = [" + person + "]");
                    if (onCallXiaoxin != null) {
                        onCallXiaoxin.onSuccessXX(person);
                    }
                }).doOnError(e -> {
                    Log.d(TAG, "getPerson doOnError() called with e = [" + e + "]");
                    if (onCallXiaoxin != null) {
                        onCallXiaoxin.onErrorXX(e);
                    }
                }).observeOn(Schedulers.io());
    }
}
