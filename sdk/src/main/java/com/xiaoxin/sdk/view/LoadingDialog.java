package com.xiaoxin.sdk.view;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.xiaoxin.sdk.R;


/**
 * Created by zhangzhi on 16/7/23.
 */
public class LoadingDialog extends Dialog {

    private LoadingDialog(@NonNull Context context) {
        this(context, null);
    }

    private LoadingDialog(@NonNull Context context, @Nullable String msg) {
        super(context, R.style.dialog_loading);
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_loading, null);
        if (msg != null) {
            ((TextView) view.findViewById(R.id.msg)).setText(msg);
        }

        setContentView(view, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        Window window = getWindow();
        if (window == null) return;
        // 设置显示动画
//		window.setWindowAnimations(R.style.main_menu_animstyle);
        WindowManager.LayoutParams wl = window.getAttributes();

        // 以下这两句是为了保证按钮可以水平满屏
        wl.width = ViewGroup.LayoutParams.WRAP_CONTENT;
        wl.height = ViewGroup.LayoutParams.WRAP_CONTENT;

        // 在底部显示
        wl.gravity = Gravity.CENTER;

        // 设置显示位置
        onWindowAttributesChanged(wl);

        // 设置点击外围解散
        setCanceledOnTouchOutside(false);
//        show();
    }

    @NonNull
    public static LoadingDialog instance(@NonNull Context c) {
        LoadingDialog dialog = new LoadingDialog(c);
        dialog.show();
        return dialog;
    }

    @NonNull
    public static LoadingDialog instance(@NonNull Context c, String msg) {
        LoadingDialog dialog = new LoadingDialog(c, msg);
        dialog.show();
        return dialog;
    }

    @NonNull
    public static LoadingDialog instanceNS(@NonNull Context c) {
        LoadingDialog dialog = new LoadingDialog(c);
        return dialog;
    }
}
