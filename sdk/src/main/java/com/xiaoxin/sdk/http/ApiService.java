package com.xiaoxin.sdk.http;


import com.xiaoxin.sdk.data.Person;
import com.xiaoxin.sdk.data.Result;
import com.xiaoxin.sdk.data.SOSRecord;

import java.util.List;

import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by zhangzhi on 16/7/23.
 */
public interface ApiService {

    /**
     * 根据唯一ID获取person对象，用于第三方设备接入系统
     *
     * @param uniqueid 设备的唯一ID，具体生成算法见 Android 与 iOS 代码
     * @param appkey   分配给第三方使用的 appkey
     */
    @GET("api/getPersonByUniqueID/{uniqueid}")
    Single<Person> getPerson(@Path("uniqueid") String uniqueid,
                             @Query("appkey") String appkey);

    @GET("api/unguardian/{id}")
    Single<Person> getWardDetail(@Path("id") String id,
                                 @Query("appkey") String appkey);

    @GET("api/sosRecord/{id}")
    Single<SOSRecord> getSOSRecord(@Path("id") String id,
                                   @Query("appkey") String appkey);

    /**
     * 根据SOSRecord获得所有的地址记录
     *
     * @param recordId SOSRecordID
     * @return List<SOSRecord>
     */
    @GET("sosRecordLocation?sort=createdAt DESC&limit=1")
    Single<List<SOSRecord>> sosRecordLocation(@Query("record") String recordId);

    @FormUrlEncoded
    @POST("api/device/bindbysosid/{sosId}")
    Single<ResponseBody> bindDevice(@Path("sosId") String sosId,
                                    @Field("appkey") String appkey,
                                    @Field("person") String person);

    @GET("api/device/list/{uniqueid}")
    Single<ResponseBody> getDeviceList(@Path("uniqueid") String uniqueid,
                                       @Query("appkey") String appkey);

    @FormUrlEncoded
    @POST("api/{uniqueid}/addContact")
    Single<Result> addContact(@Path("uniqueid") String uniqueid,
                              @Field("appkey") String appkey,
                              @Field("imei") String imei);

    @GET("api/{uniqueid}/getContacts")
    Single<List<Person>> getContacts(@Path("uniqueid") String uniqueid,
                                     @Query("appkey") String appkey);

    @GET("api/getContactInfo/{id}")
    Single<Person> getContactInfo(@Path("id") String id,
                                  @Query("appkey") String appkey);

    @FormUrlEncoded
    @POST("api/{uniqueid}/removeContact")
    Single<Result> removeContact(@Path("uniqueid") String uniqueid,
                                 @Field("appkey") String appkey,
                                 @Field("id") String id);
}
