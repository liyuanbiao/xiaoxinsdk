package com.xiaoxin.sdk.http;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.xiaoxin.sdk.Xiaoxin;
import com.xiaoxin.sdk.http.interceptor.AuthorizationInterceptor;
import com.xiaoxin.sdk.http.interceptor.SignInterceptor;
import com.xiaoxin.sdk.utils.Log;
import com.xiaoxin.sdk.utils.gson.GsonUtil;

import java.io.File;
import java.util.concurrent.TimeUnit;

import io.reactivex.schedulers.Schedulers;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by zhangzhi on 16/7/19.
 */
public final class Api {
    public static final String BASE_URL = "https://xxtserver.xiao-xin.com/";
    public static final String IMAGE_PATH = BASE_URL + "files/";

    private static final String CACHE_DIR = "HttpCache";
    private static final long TIMEOUT = 30;

    private static final String TAG = "HttpApi";
    private static final HttpLoggingInterceptor loggingInterceptor =
            new HttpLoggingInterceptor(message -> Log.println(Log.INFO, TAG, message));

    static {
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
    }

    @NonNull
    private static OkHttpClient getClient(@NonNull String data) {

        return new OkHttpClient.Builder()
                .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT, TimeUnit.SECONDS)
                .addInterceptor(new AuthorizationInterceptor())
                .addInterceptor(new SignInterceptor(data))
                .addInterceptor(loggingInterceptor)
                .cache(new Cache(new File(Xiaoxin.getContext().getCacheDir(), CACHE_DIR), 1024 * 1024 * 100))
                .build();
    }

    @NonNull
    private static ApiServiceImpl apiService = new ApiServiceImpl();

    @NonNull
    public static ApiService service() {
        return apiService;
    }

    @NonNull
    static ApiService service(@Nullable String data) {
        return service(data, GsonConverterFactory.create(GsonUtil.getDefault()));
    }

    @NonNull
    private static ApiService service(@Nullable String data,
                                      @NonNull Converter.Factory factory) {
        OkHttpClient client = getClient(data);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(factory)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build();
        return retrofit.create(ApiService.class);
    }

}
