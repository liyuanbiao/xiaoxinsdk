package com.xiaoxin.sdk.http.interceptor;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.xiaoxin.sdk.utils.SignUtil;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public final class SignInterceptor implements Interceptor {
    @Nullable
    private final String sign;

    public SignInterceptor(@NonNull String data) {
        sign = SignUtil.sign(data);
    }

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        Request request = chain.request();
        if (sign != null) {
            request = request.newBuilder()
                    .addHeader("sign", sign)
                    .build();
        }
        return chain.proceed(request);
    }
}
