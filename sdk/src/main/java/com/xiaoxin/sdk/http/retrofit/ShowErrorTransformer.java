package com.xiaoxin.sdk.http.retrofit;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.xiaoxin.sdk.utils.ToastUtil;

import io.reactivex.Single;
import io.reactivex.SingleSource;
import io.reactivex.SingleTransformer;

class ShowErrorTransformer<U> implements SingleTransformer<U, U> {

    @NonNull
    private Context context;
    @Nullable
    private String defaultText;

    ShowErrorTransformer(@NonNull Context context) {
        this.context = context.getApplicationContext();
    }

    ShowErrorTransformer(@NonNull Context context, @Nullable String defaultText) {
        this.context = context;
        this.defaultText = defaultText;
    }

    @Override
    public SingleSource<U> apply(@NonNull Single<U> upstream) {
        return upstream.compose(RxApiResponse.apiResponse()).doOnSuccess(apiResponse -> {
            ErrorBody errorBody = apiResponse.getError();
            if (errorBody != null) {
                String err = errorBody.getErr() != null ?
                        errorBody.getErr() : errorBody.getError();
                err = err != null ? err : defaultText;
                if (err != null) {
                    ToastUtil.show(err);
                }
            }
        }).map(ApiResponse::getData);
    }
}
