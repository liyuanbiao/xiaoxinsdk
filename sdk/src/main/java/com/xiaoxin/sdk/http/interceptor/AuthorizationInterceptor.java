package com.xiaoxin.sdk.http.interceptor;

import android.text.TextUtils;

import com.xiaoxin.sdk.DataManager;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public final class AuthorizationInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        String token = DataManager.getToken();
        Request request = chain.request();
        if (!TextUtils.isEmpty(token)) {
            request = request.newBuilder()
                    .addHeader("authorization", "Bearer " + token)
                    .addHeader("Accept", "*/*")
                    .build();
        }
        return chain.proceed(request);
    }
}
