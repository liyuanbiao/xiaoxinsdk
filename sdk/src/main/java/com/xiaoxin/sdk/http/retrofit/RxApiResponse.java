package com.xiaoxin.sdk.http.retrofit;

import android.content.Context;
import android.support.annotation.NonNull;

import io.reactivex.SingleTransformer;

public class RxApiResponse {

    @NonNull
    public static <U> SingleTransformer<U, ApiResponse<U, ErrorBody>> apiResponse() {
        return new ApiResponseTransformer<>();
    }

    @NonNull
    public static <U> SingleTransformer<U, U> showError(@NonNull Context context) {
        return showError(context, null);
    }

    @NonNull
    public static <U> SingleTransformer<U, U> showError(@NonNull Context context, String defaultText) {
        return new ShowErrorTransformer<>(context, defaultText);
    }
}
