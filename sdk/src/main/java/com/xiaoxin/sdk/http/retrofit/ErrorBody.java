package com.xiaoxin.sdk.http.retrofit;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

public class ErrorBody implements Parcelable {
    @SerializedName("code")
    private int code;
    @Nullable
    @SerializedName("err")
    private String err;
    @Nullable
    @SerializedName("error")
    private String error;

    public ErrorBody() {
    }

    public ErrorBody(@Nullable String err) {
        this.err = err;
    }

    public ErrorBody(int code, @Nullable String err) {
        this.code = code;
        this.err = err;
    }

    public ErrorBody(int code, @Nullable String err, @Nullable String error) {
        this.code = code;
        this.err = err;
        this.error = error;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Nullable
    public String getErr() {
        return err;
    }

    public void setErr(@Nullable String err) {
        this.err = err;
    }

    @Nullable
    public String getError() {
        return error;
    }

    public void setError(@Nullable String error) {
        this.error = error;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeInt(this.code);
        dest.writeString(this.err);
        dest.writeString(this.error);
    }

    protected ErrorBody(Parcel in) {
        this.code = in.readInt();
        this.err = in.readString();
        this.error = in.readString();
    }

    public static final Parcelable.Creator<ErrorBody> CREATOR = new Parcelable.Creator<ErrorBody>() {
        @Override
        public ErrorBody createFromParcel(@NonNull Parcel source) {
            return new ErrorBody(source);
        }

        @Override
        public ErrorBody[] newArray(int size) {
            return new ErrorBody[size];
        }
    };

    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ErrorBody errorBody = (ErrorBody) o;

        if (code != errorBody.code) return false;
        if (err != null ? !err.equals(errorBody.err) : errorBody.err != null) return false;
        return error != null ? error.equals(errorBody.error) : errorBody.error == null;
    }

    @Override
    public int hashCode() {
        int result = code;
        result = 31 * result + (err != null ? err.hashCode() : 0);
        result = 31 * result + (error != null ? error.hashCode() : 0);
        return result;
    }


}
