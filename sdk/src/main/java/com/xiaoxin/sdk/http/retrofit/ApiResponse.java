package com.xiaoxin.sdk.http.retrofit;

import android.support.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import retrofit2.HttpException;

public class ApiResponse<D, E> {
    @Expose
    @SerializedName("data")
    private D data;
    @Expose
    @SerializedName("error")
    private E error;
    private HttpException exception;

    public ApiResponse() {
    }


    public ApiResponse(D data, E error) {
        this.data = data;
        this.error = error;
    }

    public ApiResponse(D data, E error, HttpException exception) {
        this.data = data;
        this.error = error;
        this.exception = exception;
    }

    public ApiResponse(E error, HttpException exception) {
        this.error = error;
        this.exception = exception;
    }

    public D getData() {
        return data;
    }

    public void setData(D data) {
        this.data = data;
    }

    public E getError() {
        return error;
    }

    public void setError(E error) {
        this.error = error;
    }

    public HttpException getException() {
        return exception;
    }

    public void setException(HttpException exception) {
        this.exception = exception;
    }

    public boolean isSuccessful() {
        return data != null;
    }

    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ApiResponse<?, ?> that = (ApiResponse<?, ?>) o;

        if (data != null ? !data.equals(that.data) : that.data != null) return false;
        if (error != null ? !error.equals(that.error) : that.error != null) return false;
        return exception != null ? exception.equals(that.exception) : that.exception == null;
    }

    @Override
    public int hashCode() {
        int result = data != null ? data.hashCode() : 0;
        result = 31 * result + (error != null ? error.hashCode() : 0);
        result = 31 * result + (exception != null ? exception.hashCode() : 0);
        return result;
    }
}
