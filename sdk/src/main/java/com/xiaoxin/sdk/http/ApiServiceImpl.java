package com.xiaoxin.sdk.http;

import com.xiaoxin.sdk.data.Person;
import com.xiaoxin.sdk.data.Result;
import com.xiaoxin.sdk.data.SOSRecord;

import java.util.List;

import io.reactivex.Single;
import okhttp3.ResponseBody;

class ApiServiceImpl implements ApiService {

    @Override
    public Single<Person> getPerson(String uniqueid, String appkey) {
        String data = String.format("appkey=%s&uniqueid=%s", appkey, uniqueid);
        return Api.service(data).getPerson(uniqueid, appkey);
    }

    @Override
    public Single<Person> getWardDetail(String id, String appkey) {
        String data = String.format("appkey=%s&id=%s", appkey, id);
        return Api.service(data).getWardDetail(id, appkey);
    }

    @Override
    public Single<SOSRecord> getSOSRecord(String id, String appkey) {
        String data = String.format("appkey=%s&id=%s", appkey, id);
        return Api.service(data).getSOSRecord(id, appkey);
    }

    @Override
    public Single<List<SOSRecord>> sosRecordLocation(String recordId) {
        String data = String.format("record=%s", recordId);
        return Api.service(data).sosRecordLocation(recordId);
    }

    @Override
    public Single<ResponseBody> bindDevice(String sosId, String appkey, String person) {
        String data = String.format("appkey=%s&person=%s&sosId=%s", appkey, person, sosId);
        return Api.service(data).bindDevice(sosId, appkey, person);
    }

    @Override
    public Single<ResponseBody> getDeviceList(String uniqueid, String appkey) {
        String data = String.format("appkey=%s&uniqueid=%s", appkey, uniqueid);
        return Api.service(data).getDeviceList(uniqueid, appkey);
    }

    @Override
    public Single<Result> addContact(String uniqueid, String appkey, String imei) {
        String data = String.format("appkey=%s&imei=%s&uniqueid=%s", appkey, imei, uniqueid);
        return Api.service(data).addContact(uniqueid, appkey, imei);
    }

    @Override
    public Single<List<Person>> getContacts(String uniqueid, String appkey) {
        String data = String.format("appkey=%s&uniqueid=%s", appkey, uniqueid);
        return Api.service(data).getContacts(uniqueid, appkey);
    }

    @Override
    public Single<Person> getContactInfo(String id, String appkey) {
        String data = String.format("appkey=%s&id=%s", appkey, id);
        return Api.service(data).getContactInfo(id, appkey);
    }

    @Override
    public Single<Result> removeContact(String uniqueid, String appkey, String id) {
        String data = String.format("appkey=%s&id=%s&uniqueid=%s", appkey, id, uniqueid);
        return Api.service(data).removeContact(uniqueid, appkey, id);
    }
}
