package com.xiaoxin.sdk.http.retrofit;

import android.support.annotation.NonNull;

import com.xiaoxin.sdk.utils.gson.GsonUtil;

import java.io.IOException;

import io.reactivex.Single;
import io.reactivex.SingleSource;
import io.reactivex.SingleTransformer;
import retrofit2.HttpException;

class ApiResponseTransformer<D> implements SingleTransformer<D, ApiResponse<D, ErrorBody>> {
    @SuppressWarnings("unchecked")
    @Override
    public SingleSource<ApiResponse<D, ErrorBody>> apply(@NonNull Single<D> upstream) {
        return upstream.map(data -> new ApiResponse<D, ErrorBody>(data, null))
                .onErrorReturn(throwable -> {
                    if (throwable instanceof HttpException) {
                        try {
                            HttpException exception = (HttpException) throwable;
                            String err = exception.response().errorBody().string();
                            try {
                                ErrorBody error = GsonUtil.getDefault()
                                        .getAdapter(ErrorBody.class).fromJson(err);
                                return new ApiResponse<>(error, exception);
                            } catch (IOException e) {
                                return new ApiResponse<>(new ErrorBody(err), exception);
                            }
                        } catch (Exception e) {
                            throw new RuntimeException(throwable);
                        }
                    }
                    throw new RuntimeException(throwable);
                });
    }
}
