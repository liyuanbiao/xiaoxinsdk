package com.xiaoxin.sdk;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.MyLocationConfiguration;
import com.baidu.mapapi.map.MyLocationData;
import com.baidu.mapapi.model.LatLng;
import com.jakewharton.rxbinding2.view.RxView;
import com.xiaoxin.library.rxaudio.RxMediaPlayer;
import com.xiaoxin.rximlib.RxRongIMClient;
import com.xiaoxin.sdk.data.GPS;
import com.xiaoxin.sdk.data.Person;
import com.xiaoxin.sdk.data.SOSMessage;
import com.xiaoxin.sdk.data.SOSRecord;
import com.xiaoxin.sdk.http.Api;
import com.xiaoxin.sdk.utils.ErrorHandler;
import com.xiaoxin.sdk.utils.ImageLoader;
import com.xiaoxin.sdk.utils.Log;
import com.xiaoxin.sdk.utils.PermissionUtil;
import com.xiaoxin.sdk.utils.ToastUtil;
import com.xiaoxin.sdk.utils.rong.BaseRongCallListener;
import com.xiaoxin.sdk.utils.rong.CallDisconnectedException;
import com.xiaoxin.sdk.utils.rong.CallErrorCodeException;
import com.xiaoxin.sdk.utils.rx.RxLoadingDialog;
import com.xiaoxin.sdk.view.WaveformView;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.functions.Functions;
import io.reactivex.schedulers.Schedulers;
import io.rong.calllib.IRongCallListener;
import io.rong.calllib.IRongReceivedCallListener;
import io.rong.calllib.RongCallClient;
import io.rong.calllib.RongCallCommon;
import io.rong.calllib.RongCallSession;
import io.rong.imlib.model.Conversation;
import io.rong.imlib.model.Message;


public class SOSActivity extends Activity {

    public static final String ACTION_START_ALARM =
            "action.com.xiaoxin.sdk.SOSActivity.start_alarm";
    public static final String ACTION_STOP_ALARM =
            "action.com.xiaoxin.sdk.SOSActivity.stop_alarm";
    private static final String TAG = "SOSActivity";
    private static final String ACTION_FINISH_SOS_ACTIVITY =
            "action.com.xiaoxin.sdk.SOSActivity.finish";
    private static final String EXTRA_SOS = "EXTRA_SOS";
    private static final String KEY_SOS_RECORD = "SOSRecord";
    private static final String KEY_PERSON = "Person";
    private static final int REQUEST_RONG_CALL_PERMISSION = 0x100;
    private BaiduMap baiduMap;
    private MapView mMapView;
    private TextView tvName;
    private TextView tvStartTime;
    private TextView tvEndTime;
    private TextView tvAddress;
    private ImageView ivIcon;
    private ImageView ivJoin;
    private WaveformView voiceWave;
    private Button btAlarm;
    @Nullable
    private Person mPerson;
    @Nullable
    private SOSRecord mSOSRecord;
    private BitmapDescriptor mCurrentMarker;
    private boolean isConnect = false;
    @NonNull
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private Disposable alertDisposable;
    private Disposable sendSOSMessageDisposable;
    @NonNull
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action == null) {
                return;
            }
            switch (action) {
                case ACTION_START_ALARM:
                    startAlert();
                    break;
                case ACTION_STOP_ALARM:
                    stopAlert();
                    break;
                case ACTION_FINISH_SOS_ACTIVITY:
                    finish();
                    break;
            }
        }
    };

    private void addDisposable(@NonNull Disposable d) {
        compositeDisposable.add(d);
    }

    @NonNull
    private BDLocationListener bdLocationListener = new BDLocationListener() {
        @Override
        public void onReceiveLocation(BDLocation location) {

            MyLocationData locationData = new MyLocationData.Builder()
                    .accuracy(location.getRadius())
                    .direction(100)
                    .latitude(location.getLatitude())
                    .longitude(location.getLongitude())
                    .build();
            baiduMap.setMyLocationData(locationData);

            MapStatus mapStatus = new MapStatus.Builder()
                    .target(new LatLng(location.getLatitude(), location.getLongitude()))
                    .zoom(18.0f)
                    .build();

            baiduMap.animateMapStatus(MapStatusUpdateFactory.newMapStatus(mapStatus));
        }
    };

    @NonNull
    private IRongReceivedCallListener iRongReceivedCallListener = new IRongReceivedCallListener() {
        /**
         * 来电回调
         * @param callSession 通话实体
         */
        @Override
        public void onReceivedCall(RongCallSession callSession) {
            String callId = callSession.getCallId();
            Log.d(TAG, "onReceivedCall() called with: callSession = [" + callId + "]");
            RongCallClient.getInstance().acceptCall(callId);
            Log.d(TAG, "onReceivedCall acceptCall() called with: callId = [" + callId + "]");
        }

        @Override
        public void onCheckPermission(RongCallSession callSession) {
            Log.d(TAG, "onCheckPermission() called with: callSession = [" + callSession.getCallId() + "]");
            final SOSActivity context = SOSActivity.this;
            if (PermissionUtil.hasAudioCallPermission(context)) {
                String callId = callSession.getCallId();
                RongCallClient.getInstance().acceptCall(callId);
                Log.d(TAG, "onCheckPermission acceptCall() called with: callId = [" + callId + "]");
            } else {
                PermissionUtil.requestAudioCallPermissions(context, REQUEST_RONG_CALL_PERMISSION);
            }
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d(TAG, "onRequestPermissionsResult() called with: " +
                "\nrequestCode = [" + requestCode + "], " +
                "\npermissions = [" + Arrays.toString(permissions) + "], " +
                "\ngrantResults = [" + Arrays.toString(grantResults) + "]");
        if (requestCode == REQUEST_RONG_CALL_PERMISSION) {
            for (int i = 0; i < permissions.length; i++) {
                String permission = permissions[i];
                if (Manifest.permission.RECORD_AUDIO.equals(permission)) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        RongCallClient.getInstance().onPermissionGranted();
                    } else {
                        RongCallClient.getInstance().onPermissionDenied();
                    }
                }
            }
        }
    }


    @NonNull
    private IRongCallListener iRongCallListener = new BaseRongCallListener() {

        @Override
        public void onCallConnected(@NonNull RongCallSession rongCallSession, SurfaceView surfaceView) {
            super.onCallConnected(rongCallSession, surfaceView);
            isConnect = true;
            callUI();
            try {
                RongCallClient.getInstance().setEnableSpeakerphone(true);
            } catch (Exception e) {
                Log.e(TAG, "setEnableSpeakerphone: ", e);
            }
        }

        @Override
        public void onCallDisconnected(@NonNull RongCallSession rongCallSession, @NonNull RongCallCommon.CallDisconnectedReason callDisconnectedReason) {
            super.onCallDisconnected(rongCallSession, callDisconnectedReason);
            isConnect = false;
            //处理错误
            ErrorHandler.onError(new CallDisconnectedException(callDisconnectedReason, "onCallDisconnected"));

            if (callDisconnectedReason ==
                    RongCallCommon.CallDisconnectedReason.NETWORK_ERROR) {
                ToastUtil.show("网络不稳定，已断开，正在重连");
                hangUpCallUI();
                joinSOS();
            } else if (callDisconnectedReason ==
                    RongCallCommon.CallDisconnectedReason.REMOTE_HANGUP) {
                closeRecordUI();
            } else {
                hangUpCallUI();
            }
        }

        @Override
        public void onError(RongCallCommon.CallErrorCode callErrorCode) {
            super.onError(callErrorCode);
            ErrorHandler.onError(new CallErrorCodeException(callErrorCode));
        }
    };

    public static void stopAlarm() {
        Log.d(TAG, "stopAlarm() called");
        LocalBroadcastManager
                .getInstance(Xiaoxin.getContext())
                .sendBroadcast(new Intent(ACTION_STOP_ALARM));
    }

    public static void startAlarm() {
        Log.d(TAG, "startAlarm() called");
        LocalBroadcastManager
                .getInstance(Xiaoxin.getContext())
                .sendBroadcast(new Intent(ACTION_START_ALARM));
    }

    public static void startSOSActivity(String sosID) {
        Log.d(TAG, "startSOSActivity() called with: sosID = [" + sosID + "]");
        Intent intent = new Intent(Xiaoxin.getContext(), SOSActivity.class);
        intent.putExtra(EXTRA_SOS, sosID);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Xiaoxin.getContext().startActivity(intent);
    }

    public static void finishSOSActivity() {
        Log.d(TAG, "finishSOSActivity() called");
        LocalBroadcastManager.getInstance(Xiaoxin.getContext())
                .sendBroadcast(new Intent(ACTION_FINISH_SOS_ACTIVITY));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate() called");
        setContentView(R.layout.activity_sos);
        initView(savedInstanceState);
        registerSOSReceiver();

        //设置电话的监听
        initCall();

        initData(savedInstanceState);

        PermissionUtil.requestAudioCallPermissions(this, REQUEST_RONG_CALL_PERMISSION);
    }

    private void initData(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            mSOSRecord = savedInstanceState.getParcelable(KEY_SOS_RECORD);
            mPerson = savedInstanceState.getParcelable(KEY_PERSON);
        }
        Intent intent = getIntent();
        String sosId = intent.getStringExtra(EXTRA_SOS);
        bind(sosId);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        mMapView.onSaveInstanceState(outState);
        outState.putParcelable(KEY_SOS_RECORD, mSOSRecord);
        outState.putParcelable(KEY_PERSON, mPerson);
    }

    private void registerSOSReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ACTION_START_ALARM);
        intentFilter.addAction(ACTION_STOP_ALARM);
        intentFilter.addAction(ACTION_FINISH_SOS_ACTIVITY);

        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, intentFilter);
    }

    protected void initView(Bundle savedInstanceState) {
        mMapView = findViewById(R.id.mapView);
        tvName = findViewById(R.id.tv_name);
        tvStartTime = findViewById(R.id.tv_start);
        tvEndTime = findViewById(R.id.tv_end);
        tvAddress = findViewById(R.id.tv_address);
        ivIcon = findViewById(R.id.ci_icon);
        ivJoin = findViewById(R.id.iv_join);
        voiceWave = findViewById(R.id.wave);
        btAlarm = findViewById(R.id.bt_alarm);
        mCurrentMarker = BitmapDescriptorFactory.fromResource(R.drawable.marker_icon);

        Disposable joinSosDisposable = RxView.clicks(ivJoin)
                .throttleFirst(500, TimeUnit.MILLISECONDS)
                .subscribe(it -> joinSOS());
        addDisposable(joinSosDisposable);

        Disposable backDisposable = RxView.clicks(findViewById(R.id.tv_back))
                .throttleFirst(500, TimeUnit.MILLISECONDS)
                .subscribe(it -> finish());
        addDisposable(backDisposable);

        btAlarm.setOnClickListener(v -> {
            if (isAlerting()) {
                stopAlert();
            } else {
                startAlert();
            }
        });

        mMapView.onCreate(this, savedInstanceState);
        baiduMap = mMapView.getMap();
        baiduMap.setMapType(BaiduMap.MAP_TYPE_NORMAL);

        BitmapDescriptor mCurrentMarker = BitmapDescriptorFactory
                .fromResource(R.drawable.marker_icon);
        MyLocationConfiguration config = new MyLocationConfiguration(MyLocationConfiguration.LocationMode.NORMAL, true, mCurrentMarker);
        baiduMap.setMyLocationConfiguration(config);
        baiduMap.setMyLocationEnabled(true);
    }

    private boolean isSOSMessageSending() {
        return sendSOSMessageDisposable != null &&
                !sendSOSMessageDisposable.isDisposed();
    }

    private void joinSOS() {
        Log.d(TAG, "joinSOS() called");
        /*if (RongIMClient.getInstance().getCurrentConnectionStatus() !=
                RongIMClient.ConnectionStatusListener.ConnectionStatus.CONNECTED) {
            ToastUtil.show("融云服务器未连接");
            return;
        }*/
        RongCallSession callSession = RongCallClient.getInstance().getCallSession();
        if (callSession != null && callSession.getActiveTime() > 0) {
            //当前正在通话
            RongCallClient.getInstance().hangUpCall(callSession.getCallId());
            Log.d(TAG, "joinSOS: hangUpCall callId [" + callSession.getCallId() + "]");
        } else if (callSession != null || isSOSMessageSending()) {
            //通话已创建但是未在通话，或者正在发送请求加入通话的消息时
            Toast.makeText(SOSActivity.this, "正在接通电话，请稍等...", Toast.LENGTH_SHORT).show();
            Log.d(TAG, "joinSOS: calling please wait");
        } else {
            //加入通话
            //这里需要做一下点击处理
            Log.d(TAG, "joinSOS: sendSOSMessage");
            ivJoin.setClickable(false);
            String id = mSOSRecord.getPerson();
            Message myMessage = Message.obtain(id, Conversation.ConversationType.NONE, SOSMessage.obtain());
            sendSOSMessageDisposable = RxRongIMClient
                    .sendMessage(myMessage, "孝信通", "SOS求救")
                    // 发送失败后，延迟一段时间后重试，第一次0秒，第二次1秒，。。。。，最多十次
                    .retryWhen(e -> e.zipWith(Flowable.range(0, Integer.MAX_VALUE), (t1, t2) -> t2).take(10)
                            .flatMap(count -> Flowable.timer(count, TimeUnit.SECONDS)))
                    .compose(RxLoadingDialog.loading(this))
                    .subscribe((message, throwable) -> {
                        stopAlarm();
                        if (throwable != null) {
                            ivJoin.setClickable(true);
                            Toast.makeText(SOSActivity.this,
                                    "接通电话失败，请稍后再试", Toast.LENGTH_SHORT).show();
                        }
                    });
            addDisposable(sendSOSMessageDisposable);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //在activity执行onResume时执行mMapView. onResume ()，实现地图生命周期管理
        mMapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //在activity执行onPause时执行mMapView. onPause ()，实现地图生命周期管理
        mMapView.onPause();
    }

    private void upViewPerson() {
        tvName.setText(mPerson.getName());
        ImageLoader.displayImage(mPerson.getHeadImg(), ivIcon);
    }

    @SuppressLint("SimpleDateFormat")
    @NonNull
    private String formatDate(@Nullable Date date,
                              @NonNull String format) {
        if (date == null) return "";
        SimpleDateFormat sf = new SimpleDateFormat(format);
        return sf.format(date);
    }

    //接通电话的UI
    private void callUI() {
        Log.d(TAG, "callUI() called");
        ivJoin.setImageResource(R.drawable.rc_voip_hang_up);
        ivJoin.setVisibility(View.VISIBLE);
        ivJoin.setClickable(true);
        voiceWave.setVisibility(View.VISIBLE);
        voiceWave.updateAmplitude(1);
    }

    //挂断电话的UI
    private void hangUpCallUI() {
        Log.d(TAG, "hangUpCallUI() called");
        ivJoin.setImageResource(R.drawable.rc_voip_audio_answer_selector);
        ivJoin.setVisibility(View.VISIBLE);
        ivJoin.setClickable(true);
        btAlarm.setVisibility(View.GONE);
        voiceWave.setVisibility(View.GONE);
        voiceWave.upMinUI();
    }

    //结束SOS的UI
    private void closeRecordUI() {
        Log.d(TAG, "closeRecordUI() called");
        ivJoin.setVisibility(View.GONE);
        voiceWave.setVisibility(View.GONE);
        btAlarm.setVisibility(View.GONE);
        ivJoin.setClickable(false);
        tvEndTime.setText(String.format("结束：%s", formatDate(new Date(), "yyyy-MM-dd HH:mm:ss")));
    }

    private void upViewSoS() {
        tvStartTime.setText(String.format("开始：%s", formatDate(mSOSRecord.getCreatedAt(), "yyyy-MM-dd HH:mm:ss")));
        tvEndTime.setText(String.format("结束：%s", formatDate(mSOSRecord.getEndDate(), "yyyy-MM-dd HH:mm:ss")));

        GPS location = mSOSRecord.getLocation();
        if (location != null) {
            tvAddress.setText(location.address);
            moveTo(new LatLng(location.x, location.y));
        }
        if (mSOSRecord.getStatus() == SOSRecord.Status.closed) {
            closeRecordUI();
            stopAlarm();
        } else {
            hangUpCallUI();
            btAlarm.setVisibility(View.VISIBLE);
            startAlarm();
        }
    }


    private Completable getAlert() {
        return RxMediaPlayer.startPlay(this, R.raw.sosaudio)
                .retry().repeat().subscribeOn(Schedulers.io());
    }

    private void startAlert() {
        Log.d(TAG, "startAlert() called");
        btAlarm.setText("关闭报警");
        if (!isAlerting()) {
            alertDisposable = getAlert()
                    .subscribe(Functions.EMPTY_ACTION, Functions.emptyConsumer());
        }
    }

    private void stopAlert() {
        Log.d(TAG, "stopAlert() called");
        btAlarm.setText("打开报警");
        btAlarm.setVisibility(View.GONE);
        if (isAlerting()) {
            alertDisposable.dispose();
        }
    }

    private boolean isAlerting() {
        return alertDisposable != null && !alertDisposable.isDisposed();
    }

    private void bind(@NonNull final String sosId) {
        Log.d(TAG, "bind() called with: sosId = [" + sosId + "]");
        final String appKey = Xiaoxin.getAppKey();
        Disposable disposable = DataManager.getDataRepository().getPersonByUniqueID()
                //绑定设备
                .flatMap(person -> Api.service().bindDevice(sosId, appKey, person.getId()))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(this::onBindDeviceError)
                .observeOn(Schedulers.io())
                //获取SOSRecord
                .flatMap(body -> Api.service().getSOSRecord(sosId, appKey))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess(this::onGetSOSRecordSuccess)
                .observeOn(Schedulers.io())
                //获得SOS报警信息
                .flatMap(sosRecord -> Api.service().getWardDetail(sosRecord.getPerson(), appKey))
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLoadingDialog.loading(this))
                .subscribe((person, throwable) -> {
                    if (person != null) {
                        mPerson = person;
                        upViewPerson();
                    } else if (throwable != null) {
                        Log.e(TAG, "bind: error", throwable);
                    }
                });
        addDisposable(disposable);
    }

    private void onGetSOSRecordSuccess(@NonNull SOSRecord sosRecord) {
        mSOSRecord = sosRecord;
        upViewSoS();
    }

    private void onBindDeviceError(@Nullable Throwable throwable) {
        Toast.makeText(SOSActivity.this, "绑定失败", Toast.LENGTH_SHORT).show();
        finish();
    }

    //设置sos位置
    private void moveTo(@NonNull LatLng latLng) {
        float zoom = baiduMap.getMapStatus().zoom;
        if (zoom < 18.0f) {
            zoom = 18.0f;
        }
        MapStatus.Builder builder = new MapStatus.Builder();
        builder.target(latLng)
                .zoom(zoom);
        baiduMap.animateMapStatus(MapStatusUpdateFactory.newMapStatus(builder.build()));

        baiduMap.clear();
        baiduMap.addOverlay(new MarkerOptions().position(latLng)
                .icon(mCurrentMarker));

    }

    private void initCall() {
        Log.d(TAG, "initCall() called");
        RongCallClient.setReceivedCallListener(iRongReceivedCallListener);
        RongCallClient callClient = RongCallClient.getInstance();
        if (callClient != null) {
            Log.w(TAG, "initCall: callClient != null");
            callClient.setVoIPCallListener(iRongCallListener);
        } else {
            Log.w(TAG, "initCall: null");
        }
    }

    private void releaseCall() {
        RongCallClient.setReceivedCallListener(null);
        RongCallClient callClient = RongCallClient.getInstance();
        if (callClient != null) {
            callClient.hangUpCall();
            callClient.setVoIPCallListener(null);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
        hangUpCallUI();
        releaseCall();
        stopAlert();
        baiduMap.setMyLocationEnabled(false);
        //在activity执行onDestroy时执行mMapView.onDestroy()，实现地图生命周期管理
        mMapView.onDestroy();
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

}
