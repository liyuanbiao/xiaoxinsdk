package com.xiaoxin.sdk;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresPermission;
import android.text.TextUtils;

import com.baidu.mapapi.SDKInitializer;
import com.xiaoxin.rximlib.RxRongIMClient;
import com.xiaoxin.sdk.call.CallSingleObserver;
import com.xiaoxin.sdk.call.OnCallListener;
import com.xiaoxin.sdk.call.OnCallXiaoxin;
import com.xiaoxin.sdk.data.Person;
import com.xiaoxin.sdk.data.Result;
import com.xiaoxin.sdk.data.SOSMessage;
import com.xiaoxin.sdk.data.SOSRecord;
import com.xiaoxin.sdk.http.Api;
import com.xiaoxin.sdk.http.retrofit.RxApiResponse;
import com.xiaoxin.sdk.internal.AutoPlayMessageListener;
import com.xiaoxin.sdk.internal.ReceiveMessageHelper;
import com.xiaoxin.sdk.internal.rong.UserProvider;
import com.xiaoxin.sdk.utils.ErrorHandler;
import com.xiaoxin.sdk.utils.Log;
import com.xiaoxin.sdk.utils.RxUtil;
import com.xiaoxin.sdk.utils.SignUtil;
import com.xiaoxin.sdk.utils.rong.RongIMUtil;

import java.util.List;
import java.util.Objects;

import io.rong.imkit.RongIM;
import io.rong.imkit.manager.AudioRecordManager;
import io.rong.imlib.IRongCallback;
import io.rong.imlib.RongIMClient;
import io.rong.imlib.model.Conversation;
import io.rong.imlib.model.Message;
import io.rong.imlib.model.MessageContent;
import io.rong.message.TextMessage;
import io.rong.message.VoiceMessage;
import okhttp3.ResponseBody;

public class Xiaoxin {
    private static final String TAG = "Xiaoxin";

    //sdk版本名称
    public static final String VERSION = BuildConfig.VERSION_NAME;

    @SuppressLint("StaticFieldLeak")
    private static Context mContext;
    private static String mPrivateKey;
    private static String mAppKey;

    @NonNull
    public static Context getContext() {
        return mContext;
    }

    @NonNull
    public static String getAppKey() {
        return mAppKey;
    }

    @NonNull
    public static String getPrivateKey() {
        return mPrivateKey;
    }

    @NonNull
    @SuppressLint("MissingPermission")
    @RequiresPermission(android.Manifest.permission.READ_PHONE_STATE)
    public static String getUniqueid() {
        return Util.getUniqueid(mContext);
    }


    /**
     * 设置debug模式
     *
     * @param debug if true 开启 false 关闭
     */
    public static void setDebug(boolean debug) {
        Log.setDebug(debug);
    }

    /**
     * 查询是否开启debug模式
     *
     * @return true if true 开启 false 关闭
     */
    public static boolean isDebug() {
        return Log.isDebug();
    }


    /**
     * 设置是否在错误时显示 toast
     *
     * @param toastOnError true or false
     */
    public static void setToastOnError(boolean toastOnError) {
        ErrorHandler.setToastOnError(toastOnError);
    }

    /**
     * 是否在错误时显示toast
     *
     * @return true or false
     */
    public static boolean isToastOnError() {
        return ErrorHandler.isToastOnError();
    }

    /**
     * 初始化
     *
     * @param context    上下文对象
     * @param appkey     融云AppKey
     * @param privateKey 解密私钥
     */
    @SuppressLint("MissingPermission")
    @RequiresPermission(android.Manifest.permission.READ_PHONE_STATE)
    public static void init(@NonNull Context context,
                            @NonNull String appkey,
                            @NonNull String privateKey) {
        init(context, appkey, privateKey, null);
    }


    /**
     * 初始化
     *
     * @param context       上下文对象
     * @param appKey        融云AppKey
     * @param privateKey    解密私钥
     * @param onCallXiaoxin 回调
     */
    @SuppressLint("MissingPermission")
    @RequiresPermission(android.Manifest.permission.READ_PHONE_STATE)
    public static void init(@NonNull final Context context,
                            @NonNull final String appKey,
                            @NonNull final String privateKey,
                            @Nullable OnCallXiaoxin onCallXiaoxin) {
        Log.d(TAG, "init() called with:" +
                " appKey = [" + appKey + "], privateKey = [" + privateKey + "]");
        mContext = Objects.requireNonNull(context, "Context must not be null").getApplicationContext();
        mAppKey = Objects.requireNonNull(appKey, "AppKey must not be null");
        mPrivateKey = Objects.requireNonNull(privateKey, "PrivateKey must not be null");

        SignUtil.init(privateKey);

        Context ctx = mContext;

        DataManager.init(ctx, getUniqueid(), getAppKey());

        initBaiduMap(ctx);

        Util.initTts(ctx);
        if (TextUtils.equals(context.getPackageName(), Util.getCurProcessName(ctx))) {
            Util.initImageLoader(context);
            initRongCloud(ctx, appKey, onCallXiaoxin);
        }
    }

    private static void initBaiduMap(Context context) {
        Log.d(TAG, "initBaiduMap() called");
        SDKInitializer.initialize(context);
    }

    private static void initRongCloud(@NonNull Context context,
                                      @NonNull String appKey,
                                      @Nullable OnCallXiaoxin onCallXiaoxin) {
        Log.d(TAG, "initRongCloud() called with: appKey = [" + appKey + "]");
        RongIM.init(context, Util.getRongCloudAppKey());
        RongIM.registerMessageType(SOSMessage.class);
        RongIM.setUserInfoProvider(UserProvider.getInstance(), true);
        RongIM.setOnReceiveMessageListener(ReceiveMessageHelper.getInstance());
        login(appKey, onCallXiaoxin);
        if (onCallXiaoxin != null) {
            onCallXiaoxin.init();
        }
    }

    private static void login(@NonNull String appKey,
                              @Nullable final OnCallXiaoxin onCallXiaoxin) {
        Objects.requireNonNull(appKey, "login called() appKey is null");
        DataManager.login(onCallXiaoxin);
    }

    /**
     * 接口功能 - 第三方应用根据sosId与小设备进行绑定（完成绑定后小设备发出的 SOS 将可转达给第三方应用）
     *
     * @param sosId    SosRecord ID
     * @param personId 第三方应用的 Person ID（通过 getPersonByUniqueID 接口获取）
     */
    public static void bindDevice(@NonNull String sosId,
                                  @NonNull String personId,
                                  @Nullable final OnCallListener<String> onCallListener) {
        Objects.requireNonNull(sosId, "bindDevice called() sosId is null");
        Objects.requireNonNull(personId, "bindDevice called() personId is null");
        Api.service().bindDevice(sosId, getAppKey(), personId)
                .map(ResponseBody::string)
                .compose(RxApiResponse.showError(getContext()))
                .compose(RxUtil.io_main())
                .subscribe(new CallSingleObserver<>(onCallListener));
    }

    /**
     * 接口功能 - 获取被监护人信息
     *
     * @param personId       被监护人的 Person ID
     * @param onCallListener Person
     */
    public static void getSOSPerson(@NonNull String personId,
                                    @Nullable final OnCallListener<Person> onCallListener) {
        Objects.requireNonNull(personId, "getSOSPerson called() personId is null");
        Api.service().getWardDetail(personId, getAppKey())
                .doOnSuccess(RongIMUtil::refreshUserInfoCache)
                .compose(RxApiResponse.showError(getContext()))
                .compose(RxUtil.io_main())
                .subscribe(new CallSingleObserver<>(onCallListener));
    }

    /**
     * 接口功能 - 第三方应用获取与其绑定的小设备列表（即可向应用转发 SOS 的小设备列表）
     *
     * @param uniqueid       第三方设备的唯一标识
     * @param onCallListener 包括被监护人具体信息的数组 or { err: 'Error Message' }
     */
    public static void getDeviceList(@NonNull String uniqueid,
                                     @Nullable final OnCallListener<String> onCallListener) {
        Objects.requireNonNull(uniqueid, "getDeviceList called() uniqueid is null");
        Api.service().getDeviceList(uniqueid, getAppKey())
                .compose(RxApiResponse.showError(getContext()))
                .map(ResponseBody::string)
                .compose(RxUtil.io_main())
                .subscribe(new CallSingleObserver<>(onCallListener));
    }

    /**
     * 接口功能 - 获取 SosRecord 信息
     *
     * @param sosId          SosRecord ID
     * @param onCallListener 回调
     */
    public static void getSOSRecord(@NonNull String sosId,
                                    @Nullable final OnCallListener<SOSRecord> onCallListener) {
        Objects.requireNonNull(sosId, "getSOSRecord called() sosId is null");
        Api.service().getSOSRecord(sosId, getAppKey())
                .compose(RxApiResponse.showError(getContext()))
                .compose(RxUtil.io_main())
                .subscribe(new CallSingleObserver<>(onCallListener));
    }

    /**
     * 接口功能 - 第三方应用添加孝信通终端为联系人
     * 接口说明 - 第三方应用添加孝信通终端为联系人，前提条件是孝信通终端已成功开机过并进行了初始化（有提示‘登录成功’字样）
     *
     * @param imei           孝信通终端设备的 IMEI 号
     * @param uniqueid       第三方应用的唯一标识
     * @param onCallListener 回调
     */
    public static void addContact(@NonNull String imei,
                                  @NonNull String uniqueid,
                                  @Nullable final OnCallListener<Result> onCallListener) {
        Objects.requireNonNull(imei, "addContact called() imei is null");
        Objects.requireNonNull(uniqueid, "addContact called() uniqueid is null");
        Api.service().addContact(uniqueid, getAppKey(), imei)
                .compose(RxApiResponse.showError(getContext()))
                .compose(RxUtil.io_main())
                .subscribe(new CallSingleObserver<>(onCallListener));
    }

    /**
     * 接口功能 - 第三方应用获取孝信通终端联系人列表
     * 接口说明 - 当第三方应用添加孝信通终端为联系人后，可以通过此接口获取联系人列表
     *
     * @param uniqueid       第三方应用的唯一标识
     * @param onCallListener 回调
     */
    public static void getContacts(@NonNull String uniqueid,
                                   @Nullable final OnCallListener<List<Person>> onCallListener) {
        Objects.requireNonNull(uniqueid, "getContacts called() uniqueid is null");
        Api.service().getContacts(uniqueid, getAppKey())
                .doOnSuccess(RongIMUtil::refreshUserInfoCache)
                .compose(RxApiResponse.showError(getContext()))
                .compose(RxUtil.io_main())
                .subscribe(new CallSingleObserver<>(onCallListener));
    }


    /**
     * 接口功能 - 第三方应用获取孝信通终端联系人详细信息
     * 接口说明 - 通过 PersonID 查询某个联系人的详细信息
     *
     * @param id             需要查询联系人的 PersonID
     * @param onCallListener Person详情
     */
    public static void getContactInfo(@NonNull String id,
                                      @Nullable final OnCallListener<Person> onCallListener) {
        Objects.requireNonNull(id, "getContactInfo called() id is null");
        Api.service().getContactInfo(id, getAppKey())
                .doOnSuccess(RongIMUtil::refreshUserInfoCache)
                .compose(RxApiResponse.showError(getContext()))
                .compose(RxUtil.io_main())
                .subscribe(new CallSingleObserver<>(onCallListener));
    }

    /**
     * Success返回示例
     * <pre>{ result: 'success' }</>
     * Error返回示例
     * <pre>{ err: '<error message>' }</pre>
     * @param id             孝信通终端联系人的 PersonID
     * @param uniqueid       第三方应用的唯一标识
     * @param onCallListener 返回示例
     */
    public static void removeContact(@NonNull String id,
                                     @NonNull String uniqueid,
                                     @Nullable final OnCallListener<Result> onCallListener) {
        Objects.requireNonNull(id, "removeContact called() id is null");
        Objects.requireNonNull(uniqueid, "removeContact called() uniqueid is null");
        Api.service().removeContact(uniqueid, getAppKey(), id)
                .compose(RxApiResponse.showError(getContext()))
                .compose(RxUtil.io_main())
                .subscribe(new CallSingleObserver<>(onCallListener));
    }

    private static AutoPlayMessageListener autoPlayMessageListener = null;

    /**
     * 设置收到消息后是否自动播报
     *
     * @param enable true 自动播报
     */
    public static void setAutoPlayMessage(boolean enable) {
        ReceiveMessageHelper helper = ReceiveMessageHelper.getInstance();
        if (enable) {
            if (autoPlayMessageListener == null) {
                autoPlayMessageListener = new AutoPlayMessageListener();
            }
            helper.registerOnReceiveMessageListener(autoPlayMessageListener);
        } else {
            if (autoPlayMessageListener != null) {
                helper.unregisterOnReceiveMessageListener(autoPlayMessageListener);
            }
        }
    }

    public static boolean isAutoPlayMessage() {
        if (autoPlayMessageListener == null) {
            return false;
        }
        return ReceiveMessageHelper.getInstance().hasListener(autoPlayMessageListener);
    }

    /**
     * 发送文本消息
     *
     * @param id   personId
     * @param data 文本数据
     */
    public static void sendText(@NonNull String id,
                                @NonNull String data) {
        TextMessage textMessage = TextMessage.obtain(data);
        sendMessage(id, textMessage);
    }

    /**
     * 发送文本消息
     *
     * @param id   personId
     * @param data 文本数据
     */
    public static void sendText(@NonNull String id,
                                @NonNull String data,
                                @Nullable IRongCallback.ISendMessageCallback callback) {
        TextMessage textMessage = TextMessage.obtain(data);
        sendMessage(id, textMessage, callback);
    }

    /**
     * 发送声音消息
     *
     * @param id       personId
     * @param uri      声音地址
     * @param duration 声音时长
     */
    public static void sendVoice(@NonNull String id,
                                 @NonNull Uri uri,
                                 int duration) {
        VoiceMessage voiceMessage = VoiceMessage.obtain(uri, duration);
        sendMessage(id, voiceMessage);
    }

    /**
     * 发送声音消息
     *
     * @param id       personId
     * @param uri      声音地址
     * @param duration 声音时长
     */
    public static void sendVoice(@NonNull String id,
                                 @NonNull Uri uri,
                                 int duration,
                                 @Nullable IRongCallback.ISendMessageCallback callback) {
        VoiceMessage voiceMessage = VoiceMessage.obtain(uri, duration);
        sendMessage(id, voiceMessage, callback);
    }

    /**
     * AudioRecordManager 语音消息辅助类
     *
     * @return AudioRecordManager
     */
    public static AudioRecordManager getAudioRecordManager() {
        return AudioRecordManager.getInstance();
    }

    /**
     * 发送消息
     *
     * @param id             personId
     * @param messageContent 消息实体
     */

    public static void sendMessage(@NonNull String id,
                                   @NonNull MessageContent messageContent,
                                   @Nullable IRongCallback.ISendMessageCallback callback) {
        RongIMClient.getInstance().sendMessage(Conversation.ConversationType.PRIVATE, id,
                messageContent, null, null, callback);
    }

    public static void sendMessage(@NonNull String id,
                                   @NonNull MessageContent messageContent) {
        RongIMClient.getInstance().sendMessage(Conversation.ConversationType.PRIVATE, id,
                messageContent, null, null, new IRongCallback.ISendMessageCallback() {
                    @Override
                    public void onAttached(Message message) {
                        Log.d(TAG, "onAttached() called with: message = [" + message + "]");
                    }

                    @Override
                    public void onSuccess(Message message) {
                        Log.d(TAG, "onSuccess() called with: message = [" + message + "]");
                    }

                    @Override
                    public void onError(Message message, RongIMClient.ErrorCode errorCode) {
                        Log.d(TAG, "onError() called with: " +
                                "message = [" + message + "], errorCode = [" + errorCode + "]");
                    }
                });
    }

    /**
     * 获取 targetId 的N条历史消息记录。
     *
     * @param targetId       目标 Id ，这里指PersonId
     * @param count          要获取的消息数量。
     * @param onCallListener 获取历史消息记录的回调，按照时间顺序从新到旧排列。
     */
    public static void getHistoryMessages(
            @NonNull String targetId, int count,
            @Nullable OnCallListener<List<Message>> onCallListener
    ) {
        RxRongIMClient.getHistoryMessages(Conversation.ConversationType.PRIVATE,
                targetId, -1, count)
                .subscribe(new CallSingleObserver<>(onCallListener));
    }

    /**
     * 注册一个消息接收监听器
     *
     * @param onReceiveMessageListener OnReceiveMessageListener
     */
    public void registerOnReceiveMessageListener(@NonNull RongIMClient.OnReceiveMessageListener onReceiveMessageListener) {
        ReceiveMessageHelper.getInstance().registerOnReceiveMessageListener(onReceiveMessageListener);
    }

    /**
     * 取消注册一个消息接收监听器
     *
     * @param onReceiveMessageListener OnReceiveMessageListener
     */
    public void unregisterOnReceiveMessageListener(@NonNull RongIMClient.OnReceiveMessageListener onReceiveMessageListener) {
        ReceiveMessageHelper.getInstance().unregisterOnReceiveMessageListener(onReceiveMessageListener);
    }
}
